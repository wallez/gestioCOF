# GestioCOF

## Installation

### Vagrant

La façon recommandée d'installer GestioCOF sur votre machine est d'utiliser
[Vagrant](https://www.vagrantup.com/). Vagrant permet de créer une machine
virtuelle minimale sur laquelle tournera GestioCOF; ainsi on s'assure que tout
le monde à la même configuration de développement (même sous Windows !), et
l'installation se fait en une commande.

Pour utiliser Vagrant, il faut le
[télécharger](https://www.vagrantup.com/downloads.html) et l'installer. 

Si vous êtes sous Linux, votre distribution propose probablement des paquets
Vagrant dans le gestionnaire de paquets (la version sera moins récente, ce qui
peut parfois poser des problèmes de compatibilité).

Vagrant permet d'utiliser différents types de machines virtuelles; par défaut
il utilise [Virtualbox](https://www.virtualbox.org/) qu'il vous faudra
également installer.

Une fois ces dépendances installées voici quelques commandes importantes pour
gérer la machine virtuelle :

 - `vagrant up` permet de lancer la machine virtuelle. Si une machine virtuelle
   existe déjà, elle sera réutilisée; sinon, Vagrant va créer et configurer une
   nouvelle machine virtuelle pour vous (la première fois que vous lancez cette
   commande, Vagrant va télécharger une image d'Ubuntu; il vaut mieux avoir une
   connexion Internet pas trop mauvaise).

 - `vagrant suspend` permet de sauver l'état de la machine virtuelle sur le
   disque pour la relancer plus tard (y compris après un reboot) avec `vagrant
   up`

 - `vagrant halt` permet d'éteindre la machine virtuelle (par comparaison avec
   `vagrant suspend`, cela prend moins de place sur le disque car il n'y a pas
   besoin de sauver la RAM, mais la recréation avec `vagrant up` sera plus
   lente)

 - Enfin, `vagrant destroy` permet de détruire complètement la machine
   virtuelle : lors du prochain appel de `vagrant up`, elle sera réinstallée de
   zéro. *Attention, contrairement aux deux méthodes précédentes, `vagrant
   destroy` détruira irrémédiablement le contenu de votre base de données
   locale, si elle vous est d'un quelconque intérêt, réfléchissez à deux fois !*

 - `vagrant ssh` vous connecte en SSH à la machine virtuelle, dans le dossier
   où est installé GestioCOF. Vous pouvez utiliser les commandes Django
   habituelles (`manage.py runserver` etc.) pour lancer
   [le serveur en dev](#lancer-le-serveur-de-développement-standard) par
   exemple

**Le dossier avec le code de GestioCOF est partagé entre la machine virtuelle
et votre machine physique : vous pouvez donc utiliser votre éditeur favori pour
coder depuis l'extérieur de la machine virtuelle, et les changements seront
répercutés dans la machine virtuelle.**

#### Lancer le serveur de développement standard

Pour lancer le serveur de développement, il faut faire

       python manage.py runserver 0.0.0.0:8000

car par défaut Django n'écoute que sur l'adresse locale de la machine virtuelle
or vous voudrez accéder à GestioCOF depuis votre machine physique. L'url à
entrer dans le navigateur est `localhost:8000`.


#### Serveur de développement type production

Juste histoire de jouer, pas indispensable pour développer :

La VM Vagrant héberge en plus un serveur nginx configuré pour servir GestioCOF
comme en production : on utilise
[Daphne](https://github.com/django/daphne/) et `python manage.py runworker`
derrière un reverse-proxy nginx.

Ce serveur se lance tout seul et est accessible en dehors de la VM à l'url
`localhost:8080/gestion/`. Toutefois il ne se recharge pas tout seul lorsque le
code change, il faut relancer le worker avec `sudo systemctl restart
worker.service` pour visualiser la dernière version du code.


### Installation manuelle

Vous pouvez opter pour une installation manuelle plutôt que d'utiliser Vagrant,
il est fortement conseillé d'utiliser un environnement virtuel pour Python.

Il vous faudra installer pip, les librairies de développement de python ainsi
que sqlite3, un moteur de base de données léger et simple d'utilisation. Sous
Debian et dérivées (Ubuntu, ...) :

    sudo apt-get install python3-pip python3-dev sqlite3

Si vous décidez d'utiliser un environnement virtuel Python (virtualenv;
fortement conseillé), déplacez-vous dans le dossier où est installé GestioCOF
(le dossier où se trouve ce README), et créez-le maintenant :

    python3 -m venv venv

Pour l'activer, il faut faire

    . venv/bin/activate

dans le même dossier.

Vous pouvez maintenant installer les dépendances Python depuis le fichier
`requirements-devel.txt` :

    pip install -U pip
    pip install -r requirements-devel.txt

Pour terminer, copier le fichier `cof/settings/secret_example.py` vers
`cof/settings/secret.py`. Sous Linux ou Mac, préférez plutôt un lien symbolique
pour profiter de façon transparente des mises à jour du fichier:

    ln -s secret_example.py cof/settings/secret.py


#### Fin d'installation

Il ne vous reste plus qu'à initialiser les modèles de Django et peupler la base
de donnée avec les données nécessaires au bon fonctionnement de GestioCOF + des
données bidons bien pratiques pour développer avec la commande suivante :

    bash provisioning/prepare_django.sh

Vous êtes prêts à développer ! Lancer GestioCOF en faisant

    python manage.py runserver

### Mise à jour

Pour mettre à jour les paquets Python, utiliser la commande suivante :

    pip install --upgrade -r requirements-devel.txt

Pour mettre à jour les modèles après une migration, il faut ensuite faire :

    python manage.py migrate


## Documentation utilisateur

Une brève documentation utilisateur est accessible sur le
[wiki](https://git.eleves.ens.fr/cof-geek/gestioCOF/wikis/home) pour avoir une
idée de la façon dont le COF utilise GestioCOF.
