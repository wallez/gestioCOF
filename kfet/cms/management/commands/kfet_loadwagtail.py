from django.contrib.auth.models import Group
from django.core.management import call_command
from django.core.management.base import BaseCommand

from wagtail.wagtailcore.models import Page, Site


class Command(BaseCommand):
    help = "Importe des données pour Wagtail"

    def add_arguments(self, parser):
        parser.add_argument('--file', default='kfet_wagtail_17_05')

    def handle(self, *args, **options):

        self.stdout.write("Import des données wagtail")

        # Nettoyage des données initiales posées par Wagtail dans la migration
        # wagtailcore/0002

        Group.objects.filter(name__in=('Moderators', 'Editors')).delete()

        try:
            homepage = Page.objects.get(
                title="Welcome to your new Wagtail site!"
            )
            homepage.delete()
            Site.objects.filter(root_page=homepage).delete()
        except Page.DoesNotExist:
            pass

        # Import des données
        # Par défaut, il s'agit d'une copie du site K-Fêt (17-05)

        call_command('loaddata', options['file'])
