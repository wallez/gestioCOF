# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kfetcms', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kfetpage',
            name='col_count',
            field=models.CharField(blank=True, max_length=255, verbose_name='Nombre de colonnes', help_text="S'applique au page dont le contenu est scindé sur plusieurs colonnes."),
        ),
    ]
