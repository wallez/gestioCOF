from kfet.models import Article


def get_articles(request=None):
    articles = (
        Article.objects
        .filter(is_sold=True, hidden=False)
        .select_related('category')
        .order_by('category__name', 'name')
    )
    pressions, others = [], []
    for article in articles:
        if article.category.name == 'Pression':
            pressions.append(article)
        else:
            others.append(article)
    return {
        'pressions': pressions,
        'articles': others,
    }
