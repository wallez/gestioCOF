from django.apps import AppConfig


class KFetCMSAppConfig(AppConfig):
    name = 'kfet.cms'
    label = 'kfetcms'
    verbose_name = 'CMS K-Fêt'

    def ready(self):
        from . import hooks
