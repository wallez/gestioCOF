from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^raw_open$', views.raw_open,
        name='kfet.open.edit_raw_open'),
    url(r'^force_close$', views.force_close,
        name='kfet.open.edit_force_close'),
]
