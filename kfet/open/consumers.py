from ..decorators import kfet_is_team
from ..utils import DjangoJsonWebsocketConsumer, PermConsumerMixin

from .open import kfet_open


class OpenKfetConsumer(PermConsumerMixin, DjangoJsonWebsocketConsumer):
    """Consumer for K-Fêt Open.

    WS groups:
        kfet.open.base: Only carries the values visible for all users.
        kfet.open.team: Carries all values (raw status...).

    """

    def connection_groups(self, user, **kwargs):
        """Select which group the user should be connected."""
        if kfet_is_team(user):
            return ['kfet.open.team']
        return ['kfet.open.base']

    def connect(self, message, *args, **kwargs):
        """Send current status on connect."""
        super().connect(message, *args, **kwargs)
        self.send(kfet_open.export(message.user))
