from channels.routing import route_class

from . import consumers


routing = [
    route_class(consumers.OpenKfetConsumer)
]
