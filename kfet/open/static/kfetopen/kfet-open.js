var OpenWS = new KfetWebsocket({
    relative_url: "open/"
});

var OpenKfet = function(force_close_url, admin) {
    this.force_close_url = force_close_url;
    this.admin = admin;

    this.status = this.UNKNOWN;
    this.dom = {
        status_text: $('.kfetopen .status-text'),
        force_close_btn: $('.kfetopen .force-close-btn'),
        warning: $('.kfetopen .warning')
    },

    this.dom.force_close_btn.click( () => this.toggle_force_close() );
    setInterval( () => this.refresh(), this.refresh_interval * 1000);
    OpenWS.add_handler( data => this.refresh(data) );

};

OpenKfet.prototype = {
    // Status is unknown after . minutes without update.
    time_unknown: 15,
    // Maximum interval (seconds) between two UI refresh.
    refresh_interval: 20,

    // Prefix for classes describing place status.
    class_prefix: 'kfetopen-st-',
    // Set status-classes on this dom element.
    target: 'body',

    // Status
    OPENED: "opened",
    CLOSED: "closed",
    UNKNOWN: "unknown",

    // Admin status
    FAKE_CLOSED: "fake_closed",

    // Display values
    status_text: {
        opened: "ouverte",
        closed: "fermée",
        unknown: "_____"
    },
    force_text: {
        activate: "Fermer manuellement",
        deactivate: "Réouvrir la K-Fêt"
    },

    get is_recent() {
        return this.last_update && moment().diff(this.last_update, 'minute') <= this.time_unknown;
    },

    refresh: function(data) {
        if (data) {
            $.extend(this, data);
            this.last_update = moment();
        }
        if (!this.is_recent)
            this.status = this.UNKNOWN;
        this.refresh_dom();
    },

    refresh_dom: function() {
        let status = this.status;
        this.clear_class();

        this.add_class(status);
        this.dom.status_text.html(this.status_text[status]);

        // admin specific
        if (this.admin) {
            this.add_class(this.admin_status);
            if (this.force_close) {
                this.dom.warning.show().addClass('in');
                this.dom.force_close_btn.html(this.force_text['deactivate']);
            } else {
                this.dom.warning.removeClass('in').hide();
                this.dom.force_close_btn.html(this.force_text['activate']);
            }
        }
    },

    toggle_force_close: function(password) {
        $.post({
            url: this.force_close_url,
            data: {force_close: !this.force_close},
            beforeSend: function ($xhr) {
                $xhr.setRequestHeader("X-CSRFToken", csrftoken);
                if (password !== undefined)
                    $xhr.setRequestHeader("KFetPassword", password);
            }
        })
        .fail(function($xhr) {
            switch ($xhr.status) {
            case 403:
                requestAuth({'errors': {}}, this.toggle_force_close);
                break;
            }
        });
    },

    clear_class: function() {
        let re = new RegExp('(^|\\s)' + this.class_prefix + '\\S+', 'g');
        $(this.target).attr('class', (i, c) => c ? c.replace(re, '') : '');
    },

    add_class: function(status) {
        $(this.target).addClass(this.class_prefix + status);
    }
};
