from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from .open import kfet_open


TRUE_STR = ['1', 'True', 'true']


@csrf_exempt
@require_POST
def raw_open(request):
    token = request.POST.get('token')
    if token != settings.KFETOPEN_TOKEN:
        raise PermissionDenied
    raw_open = request.POST.get('raw_open') in TRUE_STR
    kfet_open.raw_open = raw_open
    kfet_open.send_ws()
    return HttpResponse()


@permission_required('kfet.can_force_close', raise_exception=True)
@require_POST
def force_close(request):
    force_close = request.POST.get('force_close') in TRUE_STR
    kfet_open.force_close = force_close
    kfet_open.send_ws()
    return HttpResponse()
