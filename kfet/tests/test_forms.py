# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.auth.models import User, Group

from kfet.forms import UserGroupForm


class UserGroupFormTests(TestCase):
    """Test suite for UserGroupForm."""

    def setUp(self):
        # create user
        self.user = User.objects.create(username="foo", password="foo")

        # create some K-Fêt groups
        prefix_name = "K-Fêt "
        names = ["Group 1", "Group 2", "Group 3"]
        self.kfet_groups = [
            Group.objects.create(name=prefix_name+name)
            for name in names
        ]

        # create a non-K-Fêt group
        self.other_group = Group.objects.create(name="Other group")

    def test_choices(self):
        """Only K-Fêt groups are selectable."""
        form = UserGroupForm(instance=self.user)
        groups_field = form.fields['groups']
        self.assertQuerysetEqual(
            groups_field.queryset,
            [repr(g) for g in self.kfet_groups],
            ordered=False,
        )

    def test_keep_others(self):
        """User stays in its non-K-Fêt groups."""
        user = self.user

        # add user to a non-K-Fêt group
        user.groups.add(self.other_group)

        # add user to some K-Fêt groups through UserGroupForm
        data = {
            'groups': [group.pk for group in self.kfet_groups],
        }
        form = UserGroupForm(data, instance=user)

        form.is_valid()
        form.save()
        self.assertQuerysetEqual(
            user.groups.all(),
            [repr(g) for g in [self.other_group] + self.kfet_groups],
            ordered=False,
        )
