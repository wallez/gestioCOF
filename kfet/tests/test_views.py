from decimal import Decimal
from unittest.mock import patch

from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.utils import timezone

from ..models import Account, OperationGroup, Checkout, Operation


class AccountTests(TestCase):
    """Account related views"""

    def setUp(self):
        # A user and its account
        self.user = User.objects.create_user(username="foobar", password="foo")
        acc = Account.objects.create(
            trigramme="FOO", cofprofile=self.user.profile
        )

        # Dummy operations and operation groups
        checkout = Checkout.objects.create(
            created_by=acc, name="checkout",
            valid_from=timezone.now(),
            valid_to=timezone.now() + timezone.timedelta(days=365)
        )
        opeg_data = [
            (timezone.now(), Decimal('10')),
            (timezone.now() - timezone.timedelta(days=3), Decimal('3')),
        ]
        OperationGroup.objects.bulk_create([
            OperationGroup(
                on_acc=acc, checkout=checkout, at=at, is_cof=False,
                amount=amount
            )
            for (at, amount) in opeg_data
        ])
        self.operation_groups = OperationGroup.objects.order_by("-amount")
        Operation.objects.create(
            group=self.operation_groups[0],
            type=Operation.PURCHASE,
            amount=Decimal('10')
        )
        Operation.objects.create(
            group=self.operation_groups[1],
            type=Operation.PURCHASE,
            amount=Decimal('3')
        )

    @patch('gestioncof.signals.messages')
    def test_account_read(self, mock_messages):
        """We can query the  "Account - Read" page."""
        client = Client()
        self.assertTrue(client.login(
            username="foobar",
            password="foo"
        ))
        resp = client.get("/k-fet/accounts/FOO")
        self.assertEqual(200, resp.status_code)
