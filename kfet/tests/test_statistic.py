# -*- coding: utf-8 -*-

from unittest.mock import patch

from django.test import TestCase, Client
from django.contrib.auth.models import User, Permission

from kfet.models import Account, Article, ArticleCategory


class TestStats(TestCase):

    @patch('gestioncof.signals.messages')
    def test_user_stats(self, mock_messages):
        """
        Checks that we can get the stat-related pages without any problem.
        """
        # We setup two users and an article. Only the first user is part of the
        # team.
        user = User.objects.create(username="Foobar")
        user.set_password("foobar")
        user.save()
        Account.objects.create(trigramme="FOO", cofprofile=user.profile)
        perm = Permission.objects.get(codename="is_team")
        user.user_permissions.add(perm)

        user2 = User.objects.create(username="Barfoo")
        user2.set_password("barfoo")
        user2.save()
        Account.objects.create(trigramme="BAR", cofprofile=user2.profile)

        article = Article.objects.create(
            name="article",
            category=ArticleCategory.objects.create(name="C")
        )

        # Each user have its own client
        client = Client()
        client.login(username="Foobar", password="foobar")
        client2 = Client()
        client2.login(username="Barfoo", password="barfoo")

        # 1. FOO should be able to get these pages but BAR receives a Forbidden
        # response
        user_urls = [
            "/k-fet/accounts/FOO/stat/operations/list",
            "/k-fet/accounts/FOO/stat/operations?{}".format(
                '&'.join(["scale=day",
                          "types=['purchase']",
                          "scale_args={'n_steps':+7,+'last':+True}",
                          "format=json"])),
            "/k-fet/accounts/FOO/stat/balance/list",
            "/k-fet/accounts/FOO/stat/balance?format=json"
        ]
        for url in user_urls:
            resp = client.get(url)
            self.assertEqual(200, resp.status_code)
            resp2 = client2.get(url)
            self.assertEqual(403, resp2.status_code)

        # 2. FOO is a member of the team and can get these pages but BAR
        # receives a Redirect response
        articles_urls = [
            "/k-fet/articles/{}/stat/sales/list".format(article.pk),
            "/k-fet/articles/{}/stat/sales".format(article.pk)
        ]
        for url in articles_urls:
            resp = client.get(url)
            self.assertEqual(200, resp.status_code)
            resp2 = client2.get(url, follow=True)
            self.assertRedirects(resp2, "/")
