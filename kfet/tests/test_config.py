# -*- coding: utf-8 -*-

from decimal import Decimal

from django.test import TestCase
from django.utils import timezone

import djconfig

from gestioncof.models import User
from kfet.config import kfet_config
from kfet.models import Account


class ConfigTest(TestCase):
    """Tests suite for kfet configuration."""

    def setUp(self):
        # load configuration as in djconfig middleware
        djconfig.reload_maybe()

    def test_get(self):
        self.assertTrue(hasattr(kfet_config, 'subvention_cof'))

    def test_subvention_cof(self):
        reduction_cof = Decimal('20')
        subvention_cof = Decimal('25')
        kfet_config.set(reduction_cof=reduction_cof)

        self.assertEqual(kfet_config.subvention_cof, subvention_cof)

    def test_set_decimal(self):
        """Test field of decimal type."""
        reduction_cof = Decimal('10')
        # IUT
        kfet_config.set(reduction_cof=reduction_cof)
        # check
        self.assertEqual(kfet_config.reduction_cof, reduction_cof)

    def test_set_modelinstance(self):
        """Test field of model instance type."""
        user = User.objects.create(username='foo_user')
        account = Account.objects.create(trigramme='FOO',
                                         cofprofile=user.profile)
        # IUT
        kfet_config.set(addcost_for=account)
        # check
        self.assertEqual(kfet_config.addcost_for, account)

    def test_set_duration(self):
        """Test field of duration type."""
        cancel_duration = timezone.timedelta(days=2, hours=4)
        # IUT
        kfet_config.set(cancel_duration=cancel_duration)
        # check
        self.assertEqual(kfet_config.cancel_duration, cancel_duration)
