# -*- coding: utf-8 -*-

from django.conf.urls import include, url
from django.contrib.auth.decorators import permission_required

from kfet import autocomplete, views
from kfet.decorators import teamkfet_required


urlpatterns = [
    url(r'^login/genericteam$', views.login_genericteam,
        name='kfet.login.genericteam'),
    url(r'^history$', views.history,
        name='kfet.history'),

    # -----
    # Account urls
    # -----

    # Account - General
    url(r'^accounts/$', views.account,
        name='kfet.account'),
    url(r'^accounts/is_validandfree$', views.account_is_validandfree_ajax,
        name='kfet.account.is_validandfree.ajax'),

    # Account - Create
    url(r'^accounts/new$', views.account_create,
        name='kfet.account.create'),
    url(r'^accounts/new_special$', views.account_create_special,
        name='kfet.account.create_special'),
    url(r'^accounts/new/user/(?P<username>.+)$', views.account_create_ajax,
        name='kfet.account.create.fromuser'),
    url(r'^accounts/new/clipper/(?P<login_clipper>[\w-]+)/(?P<fullname>.*)$',
        views.account_create_ajax,
        name='kfet.account.create.fromclipper'),
    url(r'^accounts/new/empty$', views.account_create_ajax,
        name='kfet.account.create.empty'),
    url(r'^autocomplete/account_new$', autocomplete.account_create,
        name='kfet.account.create.autocomplete'),

    # Account - Search
    url(r'^autocomplete/account_search$', autocomplete.account_search,
        name='kfet.account.search.autocomplete'),

    # Account - Read
    url(r'^accounts/(?P<trigramme>.{3})$', views.account_read,
        name='kfet.account.read'),

    # Account - Update
    url(r'^accounts/(?P<trigramme>.{3})/edit$', views.account_update,
        name='kfet.account.update'),

    # Account - Groups
    url(r'^accounts/groups$', views.account_group,
        name='kfet.account.group'),
    url(r'^accounts/groups/new$',
        permission_required('kfet.manage_perms')
        (views.AccountGroupCreate.as_view()),
        name='kfet.account.group.create'),
    url(r'^accounts/groups/(?P<pk>\d+)/edit$',
        permission_required('kfet.manage_perms')
        (views.AccountGroupUpdate.as_view()),
        name='kfet.account.group.update'),

    url(r'^accounts/negatives$',
        permission_required('kfet.view_negs')
        (views.AccountNegativeList.as_view()),
        name='kfet.account.negative'),

    # Account - Statistics
    url(r'^accounts/(?P<trigramme>.{3})/stat/operations/list$',
        views.AccountStatOperationList.as_view(),
        name='kfet.account.stat.operation.list'),
    url(r'^accounts/(?P<trigramme>.{3})/stat/operations$',
        views.AccountStatOperation.as_view(),
        name='kfet.account.stat.operation'),

    url(r'^accounts/(?P<trigramme>.{3})/stat/balance/list$',
        views.AccountStatBalanceList.as_view(),
        name='kfet.account.stat.balance.list'),
    url(r'^accounts/(?P<trigramme>.{3})/stat/balance$',
        views.AccountStatBalance.as_view(),
        name='kfet.account.stat.balance'),

    # -----
    # Checkout urls
    # -----

    # Checkout - General
    url('^checkouts/$',
        teamkfet_required(views.CheckoutList.as_view()),
        name='kfet.checkout'),
    # Checkout - Create
    url('^checkouts/new$',
        teamkfet_required(views.CheckoutCreate.as_view()),
        name='kfet.checkout.create'),
    # Checkout - Read
    url('^checkouts/(?P<pk>\d+)$',
        teamkfet_required(views.CheckoutRead.as_view()),
        name='kfet.checkout.read'),
    # Checkout - Update
    url('^checkouts/(?P<pk>\d+)/edit$',
        teamkfet_required(views.CheckoutUpdate.as_view()),
        name='kfet.checkout.update'),

    # -----
    # Checkout Statement urls
    # -----

    # Checkout Statement - General
    url('^checkouts/statements/$',
        teamkfet_required(views.CheckoutStatementList.as_view()),
        name='kfet.checkoutstatement'),
    # Checkout Statement - Create
    url('^checkouts/(?P<pk_checkout>\d+)/statements/add',
        teamkfet_required(views.CheckoutStatementCreate.as_view()),
        name='kfet.checkoutstatement.create'),
    # Checkout Statement - Update
    url('^checkouts/(?P<pk_checkout>\d+)/statements/(?P<pk>\d+)/edit',
        teamkfet_required(views.CheckoutStatementUpdate.as_view()),
        name='kfet.checkoutstatement.update'),

    # -----
    # Article urls
    # -----

    # Category - General
    url('^categories/$',
        teamkfet_required(views.CategoryList.as_view()),
        name='kfet.category'),
    # Category - Update
    url('^categories/(?P<pk>\d+)/edit$',
        teamkfet_required(views.CategoryUpdate.as_view()),
        name='kfet.category.update'),
    # Article - General
    url('^articles/$',
        teamkfet_required(views.ArticleList.as_view()),
        name='kfet.article'),
    # Article - Create
    url('^articles/new$',
        teamkfet_required(views.ArticleCreate.as_view()),
        name='kfet.article.create'),
    # Article - Read
    url('^articles/(?P<pk>\d+)$',
        teamkfet_required(views.ArticleRead.as_view()),
        name='kfet.article.read'),
    # Article - Update
    url('^articles/(?P<pk>\d+)/edit$',
        teamkfet_required(views.ArticleUpdate.as_view()),
        name='kfet.article.update'),
    # Article - Statistics
    url(r'^articles/(?P<pk>\d+)/stat/sales/list$',
        views.ArticleStatSalesList.as_view(),
        name='kfet.article.stat.sales.list'),
    url(r'^articles/(?P<pk>\d+)/stat/sales$',
        views.ArticleStatSales.as_view(),
        name='kfet.article.stat.sales'),

    # -----
    # K-Psul urls
    # -----

    url('^k-psul/$', views.kpsul, name='kfet.kpsul'),
    url('^k-psul/checkout_data$', views.kpsul_checkout_data,
        name='kfet.kpsul.checkout_data'),
    url('^k-psul/perform_operations$', views.kpsul_perform_operations,
        name='kfet.kpsul.perform_operations'),
    url('^k-psul/cancel_operations$', views.kpsul_cancel_operations,
        name='kfet.kpsul.cancel_operations'),
    url('^k-psul/articles_data', views.kpsul_articles_data,
        name='kfet.kpsul.articles_data'),
    url('^k-psul/update_addcost$', views.kpsul_update_addcost,
        name='kfet.kpsul.update_addcost'),
    url('^k-psul/get_settings$', views.kpsul_get_settings,
        name='kfet.kpsul.get_settings'),

    # -----
    # JSON urls
    # -----

    url(r'^history.json$', views.history_json,
        name='kfet.history.json'),
    url(r'^accounts/read.json$', views.account_read_json,
        name='kfet.account.read.json'),


    # -----
    # Settings urls
    # -----

    url(r'^settings/$',
        permission_required('kfet.change_settings')
        (views.SettingsList.as_view()),
        name='kfet.settings'),
    url(r'^settings/edit$',
        permission_required('kfet.change_settings')
        (views.SettingsUpdate.as_view()),
        name='kfet.settings.update'),


    # -----
    # Transfers urls
    # -----

    url(r'^transfers/$', views.transfers,
        name='kfet.transfers'),
    url(r'^transfers/new$', views.transfers_create,
        name='kfet.transfers.create'),
    url(r'^transfers/perform$', views.perform_transfers,
        name='kfet.transfers.perform'),
    url(r'^transfers/cancel$', views.cancel_transfers,
        name='kfet.transfers.cancel'),

    # -----
    # Inventories urls
    # -----

    url(r'^inventaires/$',
        teamkfet_required(views.InventoryList.as_view()),
        name='kfet.inventory'),
    url(r'^inventaires/new$', views.inventory_create,
        name='kfet.inventory.create'),
    url(r'^inventaires/(?P<pk>\d+)$',
        teamkfet_required(views.InventoryRead.as_view()),
        name='kfet.inventory.read'),

    # -----
    # Order urls
    # -----

    url(r'^orders/$',
        teamkfet_required(views.OrderList.as_view()),
        name='kfet.order'),
    url(r'^orders/(?P<pk>\d+)$',
        teamkfet_required(views.OrderRead.as_view()),
        name='kfet.order.read'),
    url(r'^orders/suppliers/(?P<pk>\d+)/edit$',
        teamkfet_required(views.SupplierUpdate.as_view()),
        name='kfet.order.supplier.update'),
    url(r'^orders/suppliers/(?P<pk>\d+)/new-order$', views.order_create,
        name='kfet.order.new'),
    url(r'^orders/(?P<pk>\d+)/to_inventory$', views.order_to_inventory,
        name='kfet.order.to_inventory'),
]

urlpatterns += [
    # K-Fêt Open urls
    url('^open/', include('kfet.open.urls')),
]
