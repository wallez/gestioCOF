# -*- coding: utf-8 -*-

from django.contrib.auth.context_processors import PermWrapper

from kfet.config import kfet_config


def auth(request):
    if hasattr(request, 'real_user'):
        return {
            'user': request.real_user,
            'perms': PermWrapper(request.real_user),
        }
    return {}


def config(request):
    return {'kfet_config': kfet_config}
