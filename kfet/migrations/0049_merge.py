# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0048_article_hidden'),
        ('kfet', '0048_default_datetime'),
    ]

    operations = [
    ]
