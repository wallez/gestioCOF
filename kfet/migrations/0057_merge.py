# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0056_change_account_meta'),
        ('kfet', '0054_update_promos'),
    ]

    operations = [
    ]
