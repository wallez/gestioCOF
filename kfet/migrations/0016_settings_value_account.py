# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0015_auto_20160807_2324'),
    ]

    operations = [
        migrations.AddField(
            model_name='settings',
            name='value_account',
            field=models.ForeignKey(to='kfet.Account', on_delete=django.db.models.deletion.PROTECT, default=None, null=True, blank=True),
        ),
    ]
