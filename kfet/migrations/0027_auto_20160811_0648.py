# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0026_auto_20160809_0810'),
    ]

    operations = [
        migrations.CreateModel(
            name='CheckoutStatement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('balance_old', models.DecimalField(decimal_places=2, max_digits=6)),
                ('balance_new', models.DecimalField(decimal_places=2, max_digits=6)),
                ('amount_taken', models.DecimalField(decimal_places=2, max_digits=6)),
                ('amount_error', models.DecimalField(decimal_places=2, max_digits=6)),
                ('at', models.DateTimeField(auto_now_add=True)),
                ('by', models.ForeignKey(to='kfet.Account', on_delete=django.db.models.deletion.PROTECT, related_name='+')),
                ('checkout', models.ForeignKey(to='kfet.Checkout', on_delete=django.db.models.deletion.PROTECT, related_name='statements')),
            ],
        ),
        migrations.RemoveField(
            model_name='statement',
            name='by',
        ),
        migrations.RemoveField(
            model_name='statement',
            name='checkout',
        ),
        migrations.DeleteModel(
            name='Statement',
        ),
    ]
