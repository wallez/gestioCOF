# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0032_auto_20160822_2350'),
    ]

    operations = [
        migrations.AddField(
            model_name='checkoutstatement',
            name='not_count',
            field=models.BooleanField(default=False),
        ),
    ]
