# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0031_auto_20160822_0523'),
    ]

    operations = [
        migrations.AddField(
            model_name='checkoutstatement',
            name='taken_001',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='checkoutstatement',
            name='taken_002',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='checkoutstatement',
            name='taken_005',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='checkoutstatement',
            name='taken_01',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='checkoutstatement',
            name='taken_02',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='checkoutstatement',
            name='taken_05',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='checkoutstatement',
            name='taken_1',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='checkoutstatement',
            name='taken_10',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='checkoutstatement',
            name='taken_100',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='checkoutstatement',
            name='taken_2',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='checkoutstatement',
            name='taken_20',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='checkoutstatement',
            name='taken_200',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='checkoutstatement',
            name='taken_5',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='checkoutstatement',
            name='taken_50',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='checkoutstatement',
            name='taken_500',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='checkoutstatement',
            name='taken_cheque',
            field=models.PositiveSmallIntegerField(default=0),
        ),
    ]
