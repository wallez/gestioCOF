# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0005_auto_20160802_2154'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='checkout',
            options={'ordering': ['-valid_to']},
        ),
        migrations.RenameField(
            model_name='account',
            old_name='frozen',
            new_name='is_frozen',
        ),
        migrations.AlterField(
            model_name='checkout',
            name='balance',
            field=models.DecimalField(max_digits=6, default=0, decimal_places=2),
        ),
    ]
