# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0025_auto_20160809_0750'),
    ]

    operations = [
        migrations.AlterField(
            model_name='settings',
            name='name',
            field=models.CharField(db_index=True, max_length=45, unique=True),
        ),
    ]
