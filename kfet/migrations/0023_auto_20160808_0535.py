# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0022_auto_20160808_0512'),
    ]

    operations = [
        migrations.RenameField(
            model_name='accountnegative',
            old_name='authorized_overdraft',
            new_name='authz_overdraft_amount',
        ),
        migrations.AddField(
            model_name='accountnegative',
            name='authz_overdraft_until',
            field=models.DateTimeField(null=True, default=None, blank=True),
        ),
    ]
