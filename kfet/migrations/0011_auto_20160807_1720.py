# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0010_auto_20160806_2343'),
    ]

    operations = [
        migrations.AlterField(
            model_name='operation',
            name='amount',
            field=models.DecimalField(decimal_places=2, max_digits=6, default=0, blank=True),
        ),
    ]
