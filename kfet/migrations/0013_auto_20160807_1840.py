# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0012_settings'),
    ]

    operations = [
        migrations.AlterField(
            model_name='settings',
            name='name',
            field=models.CharField(unique=True, max_length=45),
        ),
    ]
