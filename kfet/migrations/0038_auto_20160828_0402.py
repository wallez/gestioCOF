# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0037_auto_20160826_2333'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='inventory',
            options={'ordering': ['-at']},
        ),
        migrations.RemoveField(
            model_name='supplierarticle',
            name='box_capacity',
        ),
        migrations.RemoveField(
            model_name='supplierarticle',
            name='box_type',
        ),
        migrations.AddField(
            model_name='article',
            name='box_capacity',
            field=models.PositiveSmallIntegerField(blank=True, null=True, default=None),
        ),
        migrations.AddField(
            model_name='article',
            name='box_type',
            field=models.CharField(max_length=7, blank=True, null=True, default=None, choices=[('caisse', 'Caisse'), ('carton', 'Carton'), ('palette', 'Palette'), ('fût', 'Fût')]),
        ),
    ]
