# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0003_auto_20160802_2142'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accountnegative',
            name='balance_offset',
            field=models.DecimalField(decimal_places=2, max_digits=6, default=0),
        ),
    ]
