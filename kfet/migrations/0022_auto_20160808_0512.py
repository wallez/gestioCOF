# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0021_auto_20160808_0506'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accountnegative',
            name='authorized_overdraft',
            field=models.DecimalField(blank=True, decimal_places=2, null=True, default=None, max_digits=6),
        ),
        migrations.AlterField(
            model_name='accountnegative',
            name='balance_offset',
            field=models.DecimalField(blank=True, decimal_places=2, null=True, default=None, max_digits=6),
        ),
    ]
