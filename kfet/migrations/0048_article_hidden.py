# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0047_auto_20170104_1528'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='hidden',
            field=models.BooleanField(help_text='Si oui, ne sera pas affiché au public ; par exemple sur la carte.', default=False),
        ),
    ]
