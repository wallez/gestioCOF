# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0019_auto_20160808_0343'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accountnegative',
            name='start',
            field=models.DateTimeField(default=datetime.datetime.now, blank=True, null=True),
        ),
    ]
