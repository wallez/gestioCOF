# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0020_auto_20160808_0450'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accountnegative',
            name='start',
            field=models.DateTimeField(default=None, blank=True, null=True),
        ),
    ]
