# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0004_auto_20160802_2144'),
    ]

    operations = [
        migrations.CreateModel(
            name='GlobalPermissions',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
            ],
            options={
                'permissions': (('is_team', 'Is part of the team'),),
                'managed': False,
            },
        ),
        migrations.AlterModelOptions(
            name='account',
            options={},
        ),
    ]
