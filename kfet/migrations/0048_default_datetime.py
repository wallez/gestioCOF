# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0047_auto_20170104_1528'),
    ]

    operations = [
        migrations.AlterField(
            model_name='operationgroup',
            name='at',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='transfergroup',
            name='at',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
