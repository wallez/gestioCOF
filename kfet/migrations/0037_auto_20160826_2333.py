# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0036_auto_20160823_1910'),
    ]

    operations = [
        migrations.AlterField(
            model_name='supplierarticle',
            name='TVA',
            field=models.DecimalField(null=True, max_digits=4, decimal_places=2, default=None, blank=True),
        ),
        migrations.AlterField(
            model_name='supplierarticle',
            name='box_capacity',
            field=models.PositiveSmallIntegerField(null=True, default=None, blank=True),
        ),
        migrations.AlterField(
            model_name='supplierarticle',
            name='box_type',
            field=models.CharField(null=True, max_length=7, choices=[('caisse', 'Caisse'), ('carton', 'Carton'), ('palette', 'Palette'), ('fût', 'Fût')], default=None, blank=True),
        ),
        migrations.AlterField(
            model_name='supplierarticle',
            name='price_HT',
            field=models.DecimalField(null=True, max_digits=7, decimal_places=4, default=None, blank=True),
        ),
        migrations.AlterField(
            model_name='supplierarticle',
            name='rights',
            field=models.DecimalField(null=True, max_digits=7, decimal_places=4, default=None, blank=True),
        ),
    ]
