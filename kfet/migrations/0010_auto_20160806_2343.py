# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0009_auto_20160805_0720'),
    ]

    operations = [
        migrations.AlterField(
            model_name='operation',
            name='addcost_amount',
            field=models.DecimalField(max_digits=6, default=0, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='operationgroup',
            name='amount',
            field=models.DecimalField(max_digits=6, default=0, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='operationgroup',
            name='valid_by',
            field=models.ForeignKey(default=None, related_name='+', to='kfet.Account', blank=True, null=True, on_delete=django.db.models.deletion.PROTECT),
        ),
    ]
