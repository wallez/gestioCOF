# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def adapt_operation_types(apps, schema_editor):
    Operation = apps.get_model("kfet", "Operation")
    Operation.objects.filter(
        is_checkout=False,
        type__in=['withdraw', 'deposit']).update(type='edit')


def revert_operation_types(apps, schema_editor):
    Operation = apps.get_model("kfet", "Operation")
    edits = Operation.objects.filter(type='edit')
    edits.filter(amount__gt=0).update(type='deposit')
    edits.filter(amount__lte=0).update(type='withdraw')


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0049_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='operation',
            name='type',
            field=models.CharField(choices=[('purchase', 'Achat'), ('deposit', 'Charge'), ('withdraw', 'Retrait'), ('initial', 'Initial'), ('edit', 'Édition')], max_length=8),
        ),
        migrations.RunPython(adapt_operation_types, revert_operation_types),
        migrations.RemoveField(
            model_name='operation',
            name='is_checkout',
        ),
    ]
