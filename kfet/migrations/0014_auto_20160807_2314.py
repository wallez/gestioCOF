# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0013_auto_20160807_1840'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='globalpermissions',
            options={'permissions': (('is_team', 'Is part of the team'), ('can_perform_deposit', 'Peut effectuer une charge')), 'managed': False},
        ),
    ]
