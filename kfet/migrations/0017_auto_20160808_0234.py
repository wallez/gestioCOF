# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0016_settings_value_account'),
    ]

    operations = [
        migrations.AlterField(
            model_name='operation',
            name='addcost_amount',
            field=models.DecimalField(blank=True, null=True, decimal_places=2, default=None, max_digits=6),
        ),
    ]
