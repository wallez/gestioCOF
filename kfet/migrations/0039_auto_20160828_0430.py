# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0038_auto_20160828_0402'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='amount',
            field=models.DecimalField(default=0, decimal_places=2, max_digits=6),
        ),
        migrations.AlterField(
            model_name='orderarticle',
            name='quantity_received',
            field=models.IntegerField(default=0),
        ),
    ]
