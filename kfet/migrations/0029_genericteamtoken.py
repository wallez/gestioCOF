# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0028_auto_20160820_0146'),
    ]

    operations = [
        migrations.CreateModel(
            name='GenericTeamToken',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('token', models.CharField(unique=True, max_length=50)),
            ],
        ),
    ]
