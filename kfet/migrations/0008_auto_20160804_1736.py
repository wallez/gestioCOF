# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0007_auto_20160804_0641'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='trigramme',
            field=models.CharField(unique=True, validators=[django.core.validators.RegexValidator(regex='^[^a-z]{3}$')], db_index=True, max_length=3),
        ),
    ]
