# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0023_auto_20160808_0535'),
    ]

    operations = [
        migrations.AddField(
            model_name='settings',
            name='value_duration',
            field=models.DurationField(null=True, default=None, blank=True),
        ),
    ]
