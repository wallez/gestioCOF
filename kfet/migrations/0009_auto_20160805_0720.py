# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0008_auto_20160804_1736'),
    ]

    operations = [
        migrations.RenameField(
            model_name='operation',
            old_name='on_checkout',
            new_name='is_checkout',
        ),
        migrations.AddField(
            model_name='operation',
            name='article_nb',
            field=models.PositiveSmallIntegerField(default=None, null=True, blank=True),
        ),
    ]
