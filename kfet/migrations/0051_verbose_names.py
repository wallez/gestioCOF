# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('kfet', '0050_remove_checkout'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='is_frozen',
            field=models.BooleanField(default=False, verbose_name='est gelé'),
        ),
        migrations.AlterField(
            model_name='account',
            name='nickname',
            field=models.CharField(default='', max_length=255, verbose_name='surnom(s)', blank=True),
        ),
        migrations.AlterField(
            model_name='accountnegative',
            name='authz_overdraft_amount',
            field=models.DecimalField(max_digits=6, blank=True, default=None, null=True, verbose_name='négatif autorisé', decimal_places=2),
        ),
        migrations.AlterField(
            model_name='accountnegative',
            name='authz_overdraft_until',
            field=models.DateTimeField(default=None, null=True, verbose_name='expiration du négatif', blank=True),
        ),
        migrations.AlterField(
            model_name='accountnegative',
            name='balance_offset',
            field=models.DecimalField(blank=True, max_digits=6, help_text="Montant non compris dans l'autorisation de négatif", default=None, null=True, verbose_name='décalage de balance', decimal_places=2),
        ),
        migrations.AlterField(
            model_name='accountnegative',
            name='comment',
            field=models.CharField(blank=True, max_length=255, verbose_name='commentaire'),
        ),
        migrations.AlterField(
            model_name='article',
            name='box_capacity',
            field=models.PositiveSmallIntegerField(default=None, null=True, verbose_name='capacité du contenant', blank=True),
        ),
        migrations.AlterField(
            model_name='article',
            name='box_type',
            field=models.CharField(blank=True, max_length=7, choices=[('caisse', 'caisse'), ('carton', 'carton'), ('palette', 'palette'), ('fût', 'fût')], default=None, null=True, verbose_name='type de contenant'),
        ),
        migrations.AlterField(
            model_name='article',
            name='category',
            field=models.ForeignKey(related_name='articles', to='kfet.ArticleCategory', on_delete=django.db.models.deletion.PROTECT, verbose_name='catégorie'),
        ),
        migrations.AlterField(
            model_name='article',
            name='hidden',
            field=models.BooleanField(default=False, verbose_name='caché', help_text='Si oui, ne sera pas affiché au public ; par exemple sur la carte.'),
        ),
        migrations.AlterField(
            model_name='article',
            name='is_sold',
            field=models.BooleanField(default=True, verbose_name='en vente'),
        ),
        migrations.AlterField(
            model_name='article',
            name='name',
            field=models.CharField(max_length=45, verbose_name='nom'),
        ),
        migrations.AlterField(
            model_name='article',
            name='price',
            field=models.DecimalField(default=0, verbose_name='prix', decimal_places=2, max_digits=6),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='amount_error',
            field=models.DecimalField(max_digits=6, verbose_name="montant de l'erreur", decimal_places=2),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='amount_taken',
            field=models.DecimalField(max_digits=6, verbose_name='montant pris', decimal_places=2),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='balance_new',
            field=models.DecimalField(max_digits=6, verbose_name='nouvelle balance', decimal_places=2),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='balance_old',
            field=models.DecimalField(max_digits=6, verbose_name='ancienne balance', decimal_places=2),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='not_count',
            field=models.BooleanField(default=False, verbose_name='caisse non comptée'),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='taken_001',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='pièces de 1¢'),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='taken_002',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='pièces de 2¢'),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='taken_005',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='pièces de 5¢'),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='taken_01',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='pièces de 10¢'),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='taken_02',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='pièces de 20¢'),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='taken_05',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='pièces de 50¢'),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='taken_1',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='pièces de 1€'),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='taken_10',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='billets de 10€'),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='taken_100',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='billets de 100€'),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='taken_2',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='pièces de 2€'),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='taken_20',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='billets de 20€'),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='taken_200',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='billets de 200€'),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='taken_5',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='billets de 5€'),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='taken_50',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='billets de 50€'),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='taken_500',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='billets de 500€'),
        ),
        migrations.AlterField(
            model_name='checkoutstatement',
            name='taken_cheque',
            field=models.DecimalField(default=0, verbose_name='montant des chèques', decimal_places=2, max_digits=6),
        ),
        migrations.AlterField(
            model_name='supplier',
            name='address',
            field=models.TextField(verbose_name='adresse'),
        ),
        migrations.AlterField(
            model_name='supplier',
            name='comment',
            field=models.TextField(verbose_name='commentaire'),
        ),
        migrations.AlterField(
            model_name='supplier',
            name='email',
            field=models.EmailField(max_length=254, verbose_name='adresse mail'),
        ),
        migrations.AlterField(
            model_name='supplier',
            name='name',
            field=models.CharField(max_length=45, verbose_name='nom'),
        ),
        migrations.AlterField(
            model_name='supplier',
            name='phone',
            field=models.CharField(max_length=10, verbose_name='téléphone'),
        ),
    ]
