# -*- coding: utf-8 -*-

from .utils import DjangoJsonWebsocketConsumer, PermConsumerMixin


class KPsul(PermConsumerMixin, DjangoJsonWebsocketConsumer):
    groups = ['kfet.kpsul']
    perms_connect = ['kfet.is_team']
