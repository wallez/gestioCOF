# -*- coding: utf-8 -*-

from django.contrib import messages
from django.contrib.auth.signals import user_logged_in
from django.core.urlresolvers import reverse
from django.dispatch import receiver
from django.utils.safestring import mark_safe


@receiver(user_logged_in)
def messages_on_login(sender, request, user, **kwargs):
    if (not user.username == 'kfet_genericteam' and
            user.has_perm('kfet.is_team') and
            hasattr(request, 'GET') and
            'k-fet' in request.GET.get('next', '')):
        messages.info(request, mark_safe(
            '<a href="{}" class="genericteam" target="_blank">'
            '  Connexion en utilisateur partagé ?'
            '</a>'
            .format(reverse('kfet.login.genericteam'))
        ))
