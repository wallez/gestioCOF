# -*- coding: utf-8 -*-

from channels.routing import include, route_class

from . import consumers


routing = [
    route_class(consumers.KPsul, path=r'^/k-psul/$'),
    include('kfet.open.routing.routing', path=r'^/open'),
]
