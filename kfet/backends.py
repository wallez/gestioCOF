# -*- coding: utf-8 -*-

import hashlib

from django.contrib.auth.models import User, Permission
from gestioncof.models import CofProfile
from kfet.models import Account, GenericTeamToken


class KFetBackend(object):
    def authenticate(self, request):
        password = request.POST.get('KFETPASSWORD', '')
        password = request.META.get('HTTP_KFETPASSWORD', password)
        if not password:
            return None

        try:
            password_sha256 = (
                hashlib.sha256(password.encode('utf-8'))
                .hexdigest()
            )
            account = Account.objects.get(password=password_sha256)
            return account.cofprofile.user
        except Account.DoesNotExist:
            return None


class GenericTeamBackend(object):
    def authenticate(self, username=None, token=None):
        valid_token = GenericTeamToken.objects.get(token=token)
        if username == 'kfet_genericteam' and valid_token:
            # Création du user s'il n'existe pas déjà
            user, _ = User.objects.get_or_create(username='kfet_genericteam')
            profile, _ = CofProfile.objects.get_or_create(user=user)
            account, _ = Account.objects.get_or_create(
                    cofprofile=profile,
                    trigramme='GNR')

            # Ajoute la permission kfet.is_team à ce user
            perm_is_team = Permission.objects.get(codename='is_team')
            user.user_permissions.add(perm_is_team)

            return user
        return None

    def get_user(self, user_id):
        try:
            return (
                User.objects
                .select_related('profile__account_kfet')
                .get(pk=user_id)
            )
        except User.DoesNotExist:
            return None
