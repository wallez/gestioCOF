# -*- coding: utf-8 -*-

from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

from django.apps import AppConfig

class KFetConfig(AppConfig):
    name = 'kfet'
    verbose_name = "Application K-Fêt"

    def ready(self):
        import kfet.signals
        self.register_config()

    def register_config(self):
        import djconfig
        from kfet.forms import KFetConfigForm
        djconfig.register(KFetConfigForm)
