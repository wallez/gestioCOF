# -*- coding: utf-8 -*-

from django.contrib.auth.decorators import user_passes_test


def kfet_is_team(user):
    return user.has_perm('kfet.is_team')

teamkfet_required = user_passes_test(kfet_is_team)
