"""
Crée des utilisateurs, des articles et des opérations aléatoires
"""

import os
import random
from datetime import timedelta

from django.utils import timezone
from django.contrib.auth.models import User, Group, Permission, ContentType
from django.core.management import call_command

from gestioncof.management.base import MyBaseCommand
from gestioncof.models import CofProfile
from kfet.models import (Account, Checkout, CheckoutStatement, Supplier,
                         SupplierArticle, Article)

# Où sont stockés les fichiers json
DATA_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)),
                        'data')


class Command(MyBaseCommand):
    help = "Crée des utilisateurs, des articles et des opérations aléatoires"

    def handle(self, *args, **options):
        # ---
        # Groupes
        # ---

        Group.objects.filter(name__icontains='K-Fêt').delete()

        group_chef = Group(name="K-Fêt César")
        group_boy = Group(name="K-Fêt Légionnaire")

        group_chef.save()
        group_boy.save()

        permissions_chef = Permission.objects.filter(
                    content_type__in=ContentType.objects.filter(
                        app_label='kfet'))
        permissions_boy = Permission.objects.filter(
                    codename__in=['is_team', 'perform_deposit'])

        group_chef.permissions.add(*permissions_chef)
        group_boy.permissions.add(*permissions_boy)

        # ---
        # Comptes
        # ---

        self.stdout.write("Création des comptes K-Fêt")

        gaulois = CofProfile.objects.filter(user__last_name='Gaulois')
        gaulois_trigramme = map('{:03d}'.format, range(50))

        romains = CofProfile.objects.filter(user__last_name='Romain')
        romains_trigramme = map(lambda x: str(100+x), range(99))

        created_accounts = 0
        team_accounts = 0

        for (profile, trigramme) in zip(gaulois, gaulois_trigramme):
            account, created = Account.objects.get_or_create(
                trigramme=trigramme,
                cofprofile=profile,
                defaults={'balance': random.randint(1, 999)/10}
            )
            created_accounts += int(created)

            if profile.user.first_name == 'Abraracourcix':
                profile.user.groups.add(group_chef)

        for (profile, trigramme) in zip(romains, romains_trigramme):
            account, created = Account.objects.get_or_create(
                trigramme=trigramme,
                cofprofile=profile,
                defaults={'balance': random.randint(1, 999)/10}
            )
            created_accounts += int(created)

            if random.random() > 0.75 and created:
                profile.user.groups.add(group_boy)
                team_accounts += 1

        self.stdout.write("- {:d} comptes créés, {:d} dans l'équipe K-Fêt"
                          .format(created_accounts, team_accounts))

        # Compte liquide

        self.stdout.write("Création du compte liquide")
        liq_user, _ = User.objects.get_or_create(username='liquide')
        liq_profile, _ = CofProfile.objects.get_or_create(user=liq_user)
        liq_account, _ = Account.objects.get_or_create(cofprofile=liq_profile,
                                                       trigramme='LIQ')

        # Root account if existing

        root_profile = CofProfile.objects.filter(user__username='root')
        if root_profile.exists():
            self.stdout.write("Création du compte K-Fêt root")
            root_profile = root_profile.get()
            Account.objects.get_or_create(cofprofile=root_profile,
                                          trigramme='AAA')

        # ---
        # Caisse
        # ---

        checkout, created = Checkout.objects.get_or_create(
            created_by=Account.objects.get(trigramme='000'),
            name='Chaudron',
            defaults={
                'valid_from': timezone.now(),
                'valid_to': timezone.now() + timedelta(days=730)
            },
        )

        if created:
            CheckoutStatement.objects.create(
                by=Account.objects.get(trigramme='000'),
                checkout=checkout,
                balance_old=0,
                balance_new=0,
                amount_taken=0,
                amount_error=0
            )

        # ---
        # Fournisseur
        # ---

        supplier, created = Supplier.objects.get_or_create(name="Panoramix")
        if created:
            articles = random.sample(list(Article.objects.all()), 40)
            to_create = []
            for article in articles:
                to_create.append(SupplierArticle(
                    supplier=supplier,
                    article=article
                ))

            SupplierArticle.objects.bulk_create(to_create)

        # ---
        # Opérations
        # ---

        call_command('createopes', '100', '7', '--transfers=20')

        # ---
        # Wagtail CMS
        # ---

        call_command('kfet_loadwagtail')
