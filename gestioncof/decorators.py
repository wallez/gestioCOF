# -*- coding: utf-8 -*-

from django.contrib.auth.decorators import user_passes_test


def is_cof(user):
    try:
        profile = user.profile
        return profile.is_cof
    except:
        return False

cof_required = user_passes_test(is_cof)


def is_buro(user):
    try:
        profile = user.profile
        return profile.is_buro
    except:
        return False

buro_required = user_passes_test(is_buro)
