# -*- coding: utf-8 -*-
"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.test import TestCase

from gestioncof.models import CofProfile, User


class SimpleTest(TestCase):
    def test_delete_user(self):
        u = User(username='foo', first_name='foo', last_name='bar')

        # to each user there's a cofprofile associated
        u.save()
        self.assertTrue(CofProfile.objects.filter(user__username='foo').exists())

        # there's no point in having a cofprofile without a user associated.
        u.delete()
        self.assertFalse(CofProfile.objects.filter(user__username='foo').exists())

        # there's no point in having a user without a cofprofile associated.
        u.save()
        CofProfile.objects.get(user__username='foo').delete()
        self.assertFalse(User.objects.filter(username='foo').exists())
