from django.contrib import messages
from django.contrib.auth.signals import user_logged_in
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from django_cas_ng.signals import cas_user_authenticated


@receiver(user_logged_in)
def messages_on_out_login(request, user, **kwargs):
    if user.backend.startswith('django.contrib.auth'):
        msg = _('Connexion à GestioCOF réussie. Bienvenue {}.').format(
            user.get_short_name(),
        )
        messages.success(request, msg)


@receiver(cas_user_authenticated)
def mesagges_on_cas_login(request, user, **kwargs):
    msg = _('Connexion à GestioCOF par CAS réussie. Bienvenue {}.').format(
        user.get_short_name(),
    )
    messages.success(request, msg)
