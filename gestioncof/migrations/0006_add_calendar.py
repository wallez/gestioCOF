# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('bda', '0004_mails-rappel'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('gestioncof', '0005_encoding'),
    ]

    operations = [
        migrations.CreateModel(
            name='CalendarSubscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False,
                                        auto_created=True, primary_key=True)),
                ('token', models.UUIDField()),
                ('subscribe_to_events', models.BooleanField(default=True)),
                ('subscribe_to_my_shows', models.BooleanField(default=True)),
                ('other_shows', models.ManyToManyField(to='bda.Spectacle')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterModelOptions(
            name='custommail',
            options={'verbose_name': 'Mail personnalisable',
                     'verbose_name_plural': 'Mails personnalisables'},
        ),
        migrations.AlterModelOptions(
            name='eventoptionchoice',
            options={'verbose_name': 'Choix', 'verbose_name_plural': 'Choix'},
        ),
        migrations.AlterField(
            model_name='event',
            name='end_date',
            field=models.DateTimeField(null=True, verbose_name=b'Date de fin',
                                       blank=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='start_date',
            field=models.DateTimeField(
                null=True, verbose_name=b'Date de d\xc3\xa9but', blank=True),
        ),
    ]
