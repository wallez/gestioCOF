# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gestioncof', '0010_delete_custommail'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cofprofile',
            name='login_clipper',
            field=models.CharField(verbose_name='Login clipper', blank=True, max_length=32),
        ),
    ]
