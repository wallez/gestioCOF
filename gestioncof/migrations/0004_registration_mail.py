# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def create_mail(apps, schema_editor):
    CustomMail = apps.get_model("gestioncof", "CustomMail")
    db_alias = schema_editor.connection.alias
    if CustomMail.objects.filter(shortname="bienvenue").count() == 0:
        CustomMail.objects.using(db_alias).bulk_create([
            CustomMail(
                shortname="bienvenue",
                title="Bienvenue au COF",
                content="Mail de bienvenue au COF, envoyé automatiquement à " \
                    + "l'inscription.\n\n" \
                    + "Les balises {{ ... }} sont interprétées comme expliqué " \
                    + "ci-dessous à l'envoi.",
                comments="{{ nom }} \t fullname de la personne.\n"\
                    + "{{ prenom }} \t prénom de la personne.")
        ])


class Migration(migrations.Migration):

    dependencies = [
        ('gestioncof', '0003_event_image'),
    ]

    operations = [
        # Pas besoin de supprimer le mail lors de la migration dans l'autre
        # sens.
        migrations.RunPython(create_mail, migrations.RunPython.noop),
    ]
