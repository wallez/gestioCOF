# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gestioncof', '0002_enable_unprocessed_demandes'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='image',
            field=models.ImageField(upload_to=b'imgs/events/', null=True, verbose_name=b'Image', blank=True),
        ),
    ]
