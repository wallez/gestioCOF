# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gestioncof', '0008_py3'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Clipper',
        ),
    ]
