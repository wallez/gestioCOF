# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def forwards(apps, schema_editor):
    Profile = apps.get_model("gestioncof", "CofProfile")
    Profile.objects.update(comments="")


class Migration(migrations.Migration):

    dependencies = [
        ('gestioncof', '0007_alter_club'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clipper',
            name='fullname',
            field=models.CharField(verbose_name='Nom complet', max_length=200),
        ),
        migrations.AlterField(
            model_name='clipper',
            name='username',
            field=models.CharField(verbose_name='Identifiant', max_length=20),
        ),
        migrations.AlterField(
            model_name='cofprofile',
            name='comments',
            field=models.TextField(
                verbose_name="Commentaires visibles par l'utilisateur",
                blank=True),
        ),
        migrations.AlterField(
            model_name='cofprofile',
            name='is_cof',
            field=models.BooleanField(verbose_name='Membre du COF',
                                      default=False),
        ),
        migrations.AlterField(
            model_name='cofprofile',
            name='login_clipper',
            field=models.CharField(verbose_name='Login clipper', max_length=8,
                                   blank=True),
        ),
        migrations.AlterField(
            model_name='cofprofile',
            name='mailing_bda',
            field=models.BooleanField(verbose_name='Recevoir les mails BdA',
                                      default=False),
        ),
        migrations.AlterField(
            model_name='cofprofile',
            name='mailing_bda_revente',
            field=models.BooleanField(
                verbose_name='Recevoir les mails de revente de places BdA',
                default=False),
        ),
        migrations.AlterField(
            model_name='cofprofile',
            name='mailing_cof',
            field=models.BooleanField(verbose_name='Recevoir les mails COF',
                                      default=False),
        ),
        migrations.AlterField(
            model_name='cofprofile',
            name='occupation',
            field=models.CharField(verbose_name='Occupation',
                                   choices=[('exterieur', 'Extérieur'),
                                            ('1A', '1A'),
                                            ('2A', '2A'),
                                            ('3A', '3A'),
                                            ('4A', '4A'),
                                            ('archicube', 'Archicube'),
                                            ('doctorant', 'Doctorant'),
                                            ('CST', 'CST')],
                                   max_length=9, default='1A'),
        ),
        migrations.AlterField(
            model_name='cofprofile',
            name='petits_cours_accept',
            field=models.BooleanField(verbose_name='Recevoir des petits cours',
                                      default=False),
        ),
        migrations.AlterField(
            model_name='cofprofile',
            name='petits_cours_remarques',
            field=models.TextField(
                blank=True,
                verbose_name='Remarques et précisions pour les petits cours',
                default=''),
        ),
        migrations.AlterField(
            model_name='cofprofile',
            name='type_cotiz',
            field=models.CharField(
                verbose_name='Type de cotisation',
                choices=[('etudiant', 'Normalien étudiant'),
                         ('normalien', 'Normalien élève'),
                         ('exterieur', 'Extérieur')],
                max_length=9, default='normalien'),
        ),
        migrations.AlterField(
            model_name='custommail',
            name='comments',
            field=models.TextField(
                verbose_name='Informations contextuelles sur le mail',
                blank=True),
        ),
        migrations.AlterField(
            model_name='custommail',
            name='content',
            field=models.TextField(verbose_name='Contenu'),
        ),
        migrations.AlterField(
            model_name='custommail',
            name='title',
            field=models.CharField(verbose_name='Titre', max_length=200),
        ),
        migrations.AlterField(
            model_name='event',
            name='description',
            field=models.TextField(verbose_name='Description', blank=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='end_date',
            field=models.DateTimeField(null=True, verbose_name='Date de fin',
                                       blank=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='image',
            field=models.ImageField(upload_to='imgs/events/', null=True,
                                    verbose_name='Image', blank=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='location',
            field=models.CharField(verbose_name='Lieu', max_length=200),
        ),
        migrations.AlterField(
            model_name='event',
            name='registration_open',
            field=models.BooleanField(verbose_name='Inscriptions ouvertes',
                                      default=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='title',
            field=models.CharField(verbose_name='Titre', max_length=200),
        ),
        migrations.AlterField(
            model_name='eventcommentfield',
            name='fieldtype',
            field=models.CharField(verbose_name='Type',
                                   choices=[('text', 'Texte long'),
                                            ('char', 'Texte court')],
                                   max_length=10, default='text'),
        ),
        migrations.AlterField(
            model_name='eventcommentfield',
            name='name',
            field=models.CharField(verbose_name='Champ', max_length=200),
        ),
        migrations.AlterField(
            model_name='eventcommentvalue',
            name='content',
            field=models.TextField(null=True, verbose_name='Contenu',
                                   blank=True),
        ),
        migrations.AlterField(
            model_name='eventoption',
            name='multi_choices',
            field=models.BooleanField(verbose_name='Choix multiples',
                                      default=False),
        ),
        migrations.AlterField(
            model_name='eventoption',
            name='name',
            field=models.CharField(verbose_name='Option', max_length=200),
        ),
        migrations.AlterField(
            model_name='eventoptionchoice',
            name='value',
            field=models.CharField(verbose_name='Valeur', max_length=200),
        ),
        migrations.AlterField(
            model_name='petitcoursability',
            name='niveau',
            field=models.CharField(
                choices=[('college', 'Collège'), ('lycee', 'Lycée'),
                         ('prepa1styear', 'Prépa 1ère année / L1'),
                         ('prepa2ndyear', 'Prépa 2ème année / L2'),
                         ('licence3', 'Licence 3'),
                         ('other', 'Autre (préciser dans les commentaires)')],
                max_length=12, verbose_name='Niveau'),
        ),
        migrations.AlterField(
            model_name='petitcoursattribution',
            name='rank',
            field=models.IntegerField(verbose_name="Rang dans l'email"),
        ),
        migrations.AlterField(
            model_name='petitcoursattributioncounter',
            name='count',
            field=models.IntegerField(verbose_name="Nombre d'envois",
                                      default=0),
        ),
        migrations.AlterField(
            model_name='petitcoursdemande',
            name='niveau',
            field=models.CharField(
                verbose_name='Niveau',
                choices=[('college', 'Collège'), ('lycee', 'Lycée'),
                         ('prepa1styear', 'Prépa 1ère année / L1'),
                         ('prepa2ndyear', 'Prépa 2ème année / L2'),
                         ('licence3', 'Licence 3'),
                         ('other', 'Autre (préciser dans les commentaires)')],
                max_length=12, default=''),
        ),
        migrations.AlterField(
            model_name='survey',
            name='old',
            field=models.BooleanField(verbose_name='Archiver (sondage fini)',
                                      default=False),
        ),
        migrations.AlterField(
            model_name='survey',
            name='survey_open',
            field=models.BooleanField(verbose_name='Sondage ouvert',
                                      default=True),
        ),
        migrations.AlterField(
            model_name='survey',
            name='title',
            field=models.CharField(verbose_name='Titre', max_length=200),
        ),
        migrations.AlterField(
            model_name='surveyquestion',
            name='multi_answers',
            field=models.BooleanField(verbose_name='Choix multiples',
                                      default=False),
        ),
        migrations.AlterField(
            model_name='surveyquestion',
            name='question',
            field=models.CharField(verbose_name='Question', max_length=200),
        ),
        migrations.RunPython(forwards, migrations.RunPython.noop),
    ]
