# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Clipper',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=20, verbose_name=b'Identifiant')),
                ('fullname', models.CharField(max_length=200, verbose_name=b'Nom complet')),
            ],
        ),
        migrations.CreateModel(
            name='Club',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name=b'Nom')),
                ('description', models.TextField(verbose_name=b'Description')),
                ('membres', models.ManyToManyField(related_name='clubs', to=settings.AUTH_USER_MODEL)),
                ('respos', models.ManyToManyField(related_name='clubs_geres', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='CofProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('login_clipper', models.CharField(max_length=8, verbose_name=b'Login clipper', blank=True)),
                ('is_cof', models.BooleanField(default=False, verbose_name=b'Membre du COF')),
                ('num', models.IntegerField(default=0, verbose_name=b"Num\xc3\xa9ro d'adh\xc3\xa9rent", blank=True)),
                ('phone', models.CharField(max_length=20, verbose_name=b'T\xc3\xa9l\xc3\xa9phone', blank=True)),
                ('occupation', models.CharField(default=b'1A', max_length=9, verbose_name='Occupation', choices=[(b'exterieur', 'Ext\xe9rieur'), (b'1A', '1A'), (b'2A', '2A'), (b'3A', '3A'), (b'4A', '4A'), (b'archicube', 'Archicube'), (b'doctorant', 'Doctorant'), (b'CST', 'CST')])),
                ('departement', models.CharField(max_length=50, verbose_name='D\xe9partement', blank=True)),
                ('type_cotiz', models.CharField(default=b'normalien', max_length=9, verbose_name='Type de cotisation', choices=[(b'etudiant', 'Normalien \xe9tudiant'), (b'normalien', 'Normalien \xe9l\xe8ve'), (b'exterieur', 'Ext\xe9rieur')])),
                ('mailing_cof', models.BooleanField(default=False, verbose_name=b'Recevoir les mails COF')),
                ('mailing_bda', models.BooleanField(default=False, verbose_name=b'Recevoir les mails BdA')),
                ('mailing_bda_revente', models.BooleanField(default=False, verbose_name=b'Recevoir les mails de revente de places BdA')),
                ('comments', models.TextField(verbose_name=b'Commentaires visibles uniquement par le Buro', blank=True)),
                ('is_buro', models.BooleanField(default=False, verbose_name=b'Membre du Bur\xc3\xb4')),
                ('petits_cours_accept', models.BooleanField(default=False, verbose_name=b'Recevoir des petits cours')),
                ('petits_cours_remarques', models.TextField(default=b'', verbose_name='Remarques et pr\xe9cisions pour les petits cours', blank=True)),
                ('user', models.OneToOneField(related_name='profile', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Profil COF',
                'verbose_name_plural': 'Profils COF',
            },
        ),
        migrations.CreateModel(
            name='CustomMail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('shortname', models.SlugField()),
                ('title', models.CharField(max_length=200, verbose_name=b'Titre')),
                ('content', models.TextField(verbose_name=b'Contenu')),
                ('comments', models.TextField(verbose_name=b'Informations contextuelles sur le mail', blank=True)),
            ],
            options={
                'verbose_name': 'Mails personnalisables',
            },
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name=b'Titre')),
                ('location', models.CharField(max_length=200, verbose_name=b'Lieu')),
                ('start_date', models.DateField(null=True, verbose_name=b'Date de d\xc3\xa9but', blank=True)),
                ('end_date', models.DateField(null=True, verbose_name=b'Date de fin', blank=True)),
                ('description', models.TextField(verbose_name=b'Description', blank=True)),
                ('registration_open', models.BooleanField(default=True, verbose_name=b'Inscriptions ouvertes')),
                ('old', models.BooleanField(default=False, verbose_name=b'Archiver (\xc3\xa9v\xc3\xa9nement fini)')),
            ],
            options={
                'verbose_name': '\xc9v\xe9nement',
            },
        ),
        migrations.CreateModel(
            name='EventCommentField',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name=b'Champ')),
                ('fieldtype', models.CharField(default=b'text', max_length=10, verbose_name=b'Type', choices=[(b'text', 'Texte long'), (b'char', 'Texte court')])),
                ('default', models.TextField(verbose_name=b'Valeur par d\xc3\xa9faut', blank=True)),
                ('event', models.ForeignKey(related_name='commentfields', to='gestioncof.Event')),
            ],
            options={
                'verbose_name': 'Champ',
            },
        ),
        migrations.CreateModel(
            name='EventCommentValue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.TextField(null=True, verbose_name=b'Contenu', blank=True)),
                ('commentfield', models.ForeignKey(related_name='values', to='gestioncof.EventCommentField')),
            ],
        ),
        migrations.CreateModel(
            name='EventOption',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name=b'Option')),
                ('multi_choices', models.BooleanField(default=False, verbose_name=b'Choix multiples')),
                ('event', models.ForeignKey(related_name='options', to='gestioncof.Event')),
            ],
            options={
                'verbose_name': 'Option',
            },
        ),
        migrations.CreateModel(
            name='EventOptionChoice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=200, verbose_name=b'Valeur')),
                ('event_option', models.ForeignKey(related_name='choices', to='gestioncof.EventOption')),
            ],
            options={
                'verbose_name': 'Choix',
            },
        ),
        migrations.CreateModel(
            name='EventRegistration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('paid', models.BooleanField(default=False, verbose_name=b'A pay\xc3\xa9')),
                ('event', models.ForeignKey(to='gestioncof.Event')),
                ('filledcomments', models.ManyToManyField(to='gestioncof.EventCommentField', through='gestioncof.EventCommentValue')),
                ('options', models.ManyToManyField(to='gestioncof.EventOptionChoice')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Inscription',
            },
        ),
        migrations.CreateModel(
            name='PetitCoursAbility',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('niveau', models.CharField(max_length=12, verbose_name='Niveau', choices=[(b'college', 'Coll\xe8ge'), (b'lycee', 'Lyc\xe9e'), (b'prepa1styear', 'Pr\xe9pa 1\xe8re ann\xe9e / L1'), (b'prepa2ndyear', 'Pr\xe9pa 2\xe8me ann\xe9e / L2'), (b'licence3', 'Licence 3'), (b'other', 'Autre (pr\xe9ciser dans les commentaires)')])),
                ('agrege', models.BooleanField(default=False, verbose_name='Agr\xe9g\xe9')),
            ],
            options={
                'verbose_name': 'Comp\xe9tence petits cours',
                'verbose_name_plural': 'Comp\xe9tences des petits cours',
            },
        ),
        migrations.CreateModel(
            name='PetitCoursAttribution',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name="Date d'attribution")),
                ('rank', models.IntegerField(verbose_name=b"Rang dans l'email")),
                ('selected', models.BooleanField(default=False, verbose_name='S\xe9lectionn\xe9 par le demandeur')),
            ],
            options={
                'verbose_name': 'Attribution de petits cours',
                'verbose_name_plural': 'Attributions de petits cours',
            },
        ),
        migrations.CreateModel(
            name='PetitCoursAttributionCounter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count', models.IntegerField(default=0, verbose_name=b"Nombre d'envois")),
            ],
            options={
                'verbose_name': "Compteur d'attribution de petits cours",
                'verbose_name_plural': "Compteurs d'attributions de petits cours",
            },
        ),
        migrations.CreateModel(
            name='PetitCoursDemande',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name='Nom/pr\xe9nom')),
                ('email', models.CharField(max_length=300, verbose_name='Adresse email')),
                ('phone', models.CharField(max_length=20, verbose_name='T\xe9l\xe9phone (facultatif)', blank=True)),
                ('quand', models.CharField(help_text='Indiquez ici la p\xe9riode d\xe9sir\xe9e pour les petits cours (vacances scolaires, semaine, week-end).', max_length=300, verbose_name='Quand ?', blank=True)),
                ('freq', models.CharField(help_text='Indiquez ici la fr\xe9quence envisag\xe9e (hebdomadaire, 2 fois par semaine, ...)', max_length=300, verbose_name='Fr\xe9quence', blank=True)),
                ('lieu', models.CharField(help_text='Si vous avez avez une pr\xe9f\xe9rence sur le lieu.', max_length=300, verbose_name='Lieu (si pr\xe9f\xe9rence)', blank=True)),
                ('agrege_requis', models.BooleanField(default=False, verbose_name='Agr\xe9g\xe9 requis')),
                ('niveau', models.CharField(default=b'', max_length=12, verbose_name='Niveau', choices=[(b'college', 'Coll\xe8ge'), (b'lycee', 'Lyc\xe9e'), (b'prepa1styear', 'Pr\xe9pa 1\xe8re ann\xe9e / L1'), (b'prepa2ndyear', 'Pr\xe9pa 2\xe8me ann\xe9e / L2'), (b'licence3', 'Licence 3'), (b'other', 'Autre (pr\xe9ciser dans les commentaires)')])),
                ('remarques', models.TextField(verbose_name='Remarques et pr\xe9cisions', blank=True)),
                ('traitee', models.BooleanField(default=False, verbose_name='Trait\xe9e')),
                ('processed', models.DateTimeField(verbose_name='Date de traitement', blank=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Date de cr\xe9ation')),
            ],
            options={
                'verbose_name': 'Demande de petits cours',
                'verbose_name_plural': 'Demandes de petits cours',
            },
        ),
        migrations.CreateModel(
            name='PetitCoursSubject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30, verbose_name='Mati\xe8re')),
                ('users', models.ManyToManyField(related_name='petits_cours_matieres', through='gestioncof.PetitCoursAbility', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Mati\xe8re de petits cours',
                'verbose_name_plural': 'Mati\xe8res des petits cours',
            },
        ),
        migrations.CreateModel(
            name='Survey',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name=b'Titre')),
                ('details', models.TextField(verbose_name=b'D\xc3\xa9tails', blank=True)),
                ('survey_open', models.BooleanField(default=True, verbose_name=b'Sondage ouvert')),
                ('old', models.BooleanField(default=False, verbose_name=b'Archiver (sondage fini)')),
            ],
            options={
                'verbose_name': 'Sondage',
            },
        ),
        migrations.CreateModel(
            name='SurveyAnswer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'verbose_name': 'R\xe9ponses',
            },
        ),
        migrations.CreateModel(
            name='SurveyQuestion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.CharField(max_length=200, verbose_name=b'Question')),
                ('multi_answers', models.BooleanField(default=False, verbose_name=b'Choix multiples')),
                ('survey', models.ForeignKey(related_name='questions', to='gestioncof.Survey')),
            ],
            options={
                'verbose_name': 'Question',
            },
        ),
        migrations.CreateModel(
            name='SurveyQuestionAnswer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('answer', models.CharField(max_length=200, verbose_name=b'R\xc3\xa9ponse')),
                ('survey_question', models.ForeignKey(related_name='answers', to='gestioncof.SurveyQuestion')),
            ],
            options={
                'verbose_name': 'R\xe9ponse',
            },
        ),
        migrations.AddField(
            model_name='surveyanswer',
            name='answers',
            field=models.ManyToManyField(related_name='selected_by', to='gestioncof.SurveyQuestionAnswer'),
        ),
        migrations.AddField(
            model_name='surveyanswer',
            name='survey',
            field=models.ForeignKey(to='gestioncof.Survey'),
        ),
        migrations.AddField(
            model_name='surveyanswer',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='petitcoursdemande',
            name='matieres',
            field=models.ManyToManyField(related_name='demandes', verbose_name='Mati\xe8res', to='gestioncof.PetitCoursSubject'),
        ),
        migrations.AddField(
            model_name='petitcoursdemande',
            name='traitee_par',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='petitcoursattributioncounter',
            name='matiere',
            field=models.ForeignKey(verbose_name='Matiere', to='gestioncof.PetitCoursSubject'),
        ),
        migrations.AddField(
            model_name='petitcoursattributioncounter',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='petitcoursattribution',
            name='demande',
            field=models.ForeignKey(verbose_name='Demande', to='gestioncof.PetitCoursDemande'),
        ),
        migrations.AddField(
            model_name='petitcoursattribution',
            name='matiere',
            field=models.ForeignKey(verbose_name='Mati\xe8re', to='gestioncof.PetitCoursSubject'),
        ),
        migrations.AddField(
            model_name='petitcoursattribution',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='petitcoursability',
            name='matiere',
            field=models.ForeignKey(verbose_name='Mati\xe8re', to='gestioncof.PetitCoursSubject'),
        ),
        migrations.AddField(
            model_name='petitcoursability',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='eventcommentvalue',
            name='registration',
            field=models.ForeignKey(related_name='comments', to='gestioncof.EventRegistration'),
        ),
        migrations.AlterUniqueTogether(
            name='surveyanswer',
            unique_together=set([('user', 'survey')]),
        ),
        migrations.AlterUniqueTogether(
            name='eventregistration',
            unique_together=set([('user', 'event')]),
        ),
    ]
