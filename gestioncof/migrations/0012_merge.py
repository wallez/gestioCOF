# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gestioncof', '0011_remove_cofprofile_num'),
        ('gestioncof', '0011_longer_clippers'),
    ]

    operations = [
    ]
