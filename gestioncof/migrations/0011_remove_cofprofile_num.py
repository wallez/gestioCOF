# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gestioncof', '0010_delete_custommail'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cofprofile',
            name='num',
        ),
    ]
