# -*- coding: utf-8 -*-

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gestioncof', '0009_delete_clipper'),
    ]

    operations = [
        migrations.DeleteModel(
            name='CustomMail',
        ),
    ]
