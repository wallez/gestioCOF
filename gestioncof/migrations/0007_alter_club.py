# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('gestioncof', '0006_add_calendar'),
    ]

    operations = [
        migrations.AlterField(
            model_name='club',
            name='name',
            field=models.CharField(unique=True, max_length=200,
                                   verbose_name='Nom')
        ),
        migrations.AlterField(
            model_name='club',
            name='description',
            field=models.TextField(verbose_name='Description', blank=True)
        ),
        migrations.AlterField(
            model_name='club',
            name='membres',
            field=models.ManyToManyField(related_name='clubs',
                                         to=settings.AUTH_USER_MODEL,
                                         blank=True),
        ),
        migrations.AlterField(
            model_name='club',
            name='respos',
            field=models.ManyToManyField(related_name='clubs_geres',
                                         to=settings.AUTH_USER_MODEL,
                                         blank=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='start_date',
            field=models.DateTimeField(null=True,
                                       verbose_name='Date de d\xe9but',
                                       blank=True),
        ),
    ]
