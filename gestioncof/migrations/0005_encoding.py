# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gestioncof', '0004_registration_mail'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='custommail',
            options={'verbose_name': 'Mail personnalisable', 'verbose_name_plural': 'Mails personnalisables'},
        ),
        migrations.AlterModelOptions(
            name='eventoptionchoice',
            options={'verbose_name': 'Choix', 'verbose_name_plural': 'Choix'},
        ),
        migrations.AlterField(
            model_name='cofprofile',
            name='is_buro',
            field=models.BooleanField(default=False, verbose_name='Membre du Bur\xf4'),
        ),
        migrations.AlterField(
            model_name='cofprofile',
            name='num',
            field=models.IntegerField(default=0, verbose_name="Num\xe9ro d'adh\xe9rent", blank=True),
        ),
        migrations.AlterField(
            model_name='cofprofile',
            name='phone',
            field=models.CharField(max_length=20, verbose_name='T\xe9l\xe9phone', blank=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='old',
            field=models.BooleanField(default=False, verbose_name='Archiver (\xe9v\xe9nement fini)'),
        ),
        migrations.AlterField(
            model_name='event',
            name='start_date',
            field=models.DateField(null=True, verbose_name='Date de d\xe9but', blank=True),
        ),
        migrations.AlterField(
            model_name='eventcommentfield',
            name='default',
            field=models.TextField(verbose_name='Valeur par d\xe9faut', blank=True),
        ),
        migrations.AlterField(
            model_name='eventregistration',
            name='paid',
            field=models.BooleanField(default=False, verbose_name='A pay\xe9'),
        ),
        migrations.AlterField(
            model_name='survey',
            name='details',
            field=models.TextField(verbose_name='D\xe9tails', blank=True),
        ),
        migrations.AlterField(
            model_name='surveyquestionanswer',
            name='answer',
            field=models.CharField(max_length=200, verbose_name='R\xe9ponse'),
        ),
    ]
