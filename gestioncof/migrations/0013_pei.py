# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gestioncof', '0012_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cofprofile',
            name='occupation',
            field=models.CharField(
                verbose_name='Occupation',
                max_length=9,
                default='1A',
                choices=[
                    ('exterieur', 'Extérieur'),
                    ('1A', '1A'),
                    ('2A', '2A'),
                    ('3A', '3A'),
                    ('4A', '4A'),
                    ('archicube', 'Archicube'),
                    ('doctorant', 'Doctorant'),
                    ('CST', 'CST'),
                    ('PEI', 'PEI')
                ]),
        ),
        migrations.AlterField(
            model_name='cofprofile',
            name='type_cotiz',
            field=models.CharField(
                verbose_name='Type de cotisation',
                max_length=9,
                default='normalien',
                choices=[
                      ('etudiant', 'Normalien étudiant'),
                      ('normalien', 'Normalien élève'),
                      ('exterieur', 'Extérieur'),
                      ('gratis', 'Gratuit')
                ]),
        ),
    ]
