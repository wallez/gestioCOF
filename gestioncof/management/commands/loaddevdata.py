"""
Charge des données de test dans la BDD
- Utilisateurs
- Sondage
- Événement
- Petits cours
"""


import os
import random

from django.contrib.auth.models import User
from django.core.management import call_command

from gestioncof.management.base import MyBaseCommand
from gestioncof.petits_cours_models import (
    PetitCoursAbility, PetitCoursSubject, LEVELS_CHOICES,
    PetitCoursAttributionCounter
)

# Où sont stockés les fichiers json
DATA_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)),
                        'data')


class Command(MyBaseCommand):
    help = "Charge des données de test dans la BDD"

    def add_arguments(self, parser):
        """
        Permet de ne pas créer l'utilisateur "root".
        """
        parser.add_argument(
            '--no-root',
            action='store_true',
            dest='no-root',
            default=False,
            help='Ne crée pas l\'utilisateur "root"'
        )

    def handle(self, *args, **options):
        # ---
        # Utilisateurs
        # ---

        # Gaulois
        gaulois = self.from_json('gaulois.json', DATA_DIR, User)
        for user in gaulois:
            user.profile.is_cof = True
            user.profile.save()

        # Romains
        self.from_json('romains.json', DATA_DIR, User)

        # Root
        no_root = options.get('no-root', False)
        if not no_root:
            self.stdout.write("Création de l'utilisateur root")
            root, _ = User.objects.get_or_create(
                username='root',
                first_name='super',
                last_name='user',
                email='root@localhost')
            root.set_password('root')
            root.is_staff = True
            root.is_superuser = True
            root.profile.is_cof = True
            root.profile.is_buro = True
            root.profile.save()
            root.save()

        # ---
        # Petits cours
        # ---

        self.stdout.write("Inscriptions au système des petits cours")
        levels = [id for (id, verbose) in LEVELS_CHOICES]
        subjects = list(PetitCoursSubject.objects.all())
        nb_of_teachers = 0
        for user in gaulois:
            if random.randint(0, 1):
                nb_of_teachers += 1
                # L'utilisateur reçoit les demandes de petits cours
                user.profile.petits_cours_accept = True
                user.save()
                # L'utilisateur est compétent dans une matière
                subject = random.choice(subjects)
                if not PetitCoursAbility.objects.filter(
                        user=user,
                        matiere=subject).exists():
                    PetitCoursAbility.objects.create(
                        user=user,
                        matiere=subject,
                        niveau=random.choice(levels),
                        agrege=bool(random.randint(0, 1))
                    )
                    # On initialise son compteur d'attributions
                    PetitCoursAttributionCounter.objects.get_or_create(
                        user=user,
                        matiere=subject
                    )
        self.stdout.write("- {:d} inscriptions".format(nb_of_teachers))

        # ---
        # Le BdA
        # ---

        call_command('loadbdadevdata')

        # ---
        # La K-Fêt
        # ---

        call_command('loadkfetdevdata')
