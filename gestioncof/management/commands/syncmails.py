# -*- coding: utf-8 -*-
"""
Import des mails de GestioCOF dans la base de donnée
"""

import json
import os
from custommail.models import Type, CustomMail, Variable

from django.core.management.base import BaseCommand
from django.contrib.contenttypes.models import ContentType


class Command(BaseCommand):
    help = ("Va chercher les données mails de GestioCOF stocké au format json "
            "dans /gestioncof/management/data/custommails.json. Le format des "
            "données est celui donné par la commande :"
            " `python manage.py dumpdata custommail --natural-foreign` "
            "La bonne façon de mettre à jour ce fichier est donc de le "
            "charger à l'aide de syncmails, le faire les modifications à "
            "l'aide de l'interface administration et/ou du shell puis de le "
            "remplacer par le nouveau résultat de la commande précédente.")

    def handle(self, *args, **options):
        path = os.path.join(
            os.path.dirname(os.path.dirname(__file__)),
            'data', 'custommail.json')
        with open(path, 'r') as jsonfile:
            mail_data = json.load(jsonfile)

        # On se souvient à quel objet correspond quel pk du json
        assoc = {'types': {}, 'mails': {}}
        status = {'synced': 0, 'unchanged': 0}

        for obj in mail_data:
            fields = obj['fields']

            # Pour les trois types d'objets :
            # - On récupère les objets référencés par les clefs étrangères
            # - On crée l'objet si nécessaire
            # - On le stocke éventuellement dans les deux dictionnaires définis
            #   plus haut

            # Variable types
            if obj['model'] == 'custommail.variabletype':
                fields['inner1'] = assoc['types'].get(fields['inner1'])
                fields['inner2'] = assoc['types'].get(fields['inner2'])
                if fields['kind'] == 'model':
                    fields['content_type'] = (
                        ContentType.objects
                        .get_by_natural_key(*fields['content_type'])
                    )
                var_type, _ = Type.objects.get_or_create(**fields)
                assoc['types'][obj['pk']] = var_type

            # Custom mails
            if obj['model'] == 'custommail.custommail':
                mail = None
                try:
                    mail = CustomMail.objects.get(
                        shortname=fields['shortname'])
                    status['unchanged'] += 1
                except CustomMail.DoesNotExist:
                    mail = CustomMail.objects.create(**fields)
                    status['synced'] += 1
                    self.stdout.write(
                        'SYNCED {:s}'.format(fields['shortname']))
                assoc['mails'][obj['pk']] = mail

            # Variables
            if obj['model'] == 'custommail.custommailvariable':
                fields['custommail'] = assoc['mails'].get(fields['custommail'])
                fields['type'] = assoc['types'].get(fields['type'])
                try:
                    Variable.objects.get(
                        custommail=fields['custommail'],
                        name=fields['name']
                    )
                except Variable.DoesNotExist:
                    Variable.objects.create(**fields)

        # C'est agréable d'avoir le résultat affiché
        self.stdout.write(
            '{synced:d} mails synchronized {unchanged:d} unchanged'
            .format(**status)
        )
