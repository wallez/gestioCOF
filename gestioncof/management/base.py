"""
Un mixin à utiliser avec BaseCommand pour charger des objets depuis un json
"""

import os
import json

from django.core.management.base import BaseCommand


class MyBaseCommand(BaseCommand):
    """
    Ajoute une méthode ``from_json`` qui charge des objets à partir d'un json.
    """

    def from_json(self, filename, data_dir, klass,
                  callback=lambda obj: obj):
        """
        Charge les objets contenus dans le fichier json référencé par
        ``filename`` dans la base de donnée. La fonction callback est appelées
        sur chaque objet avant enregistrement.
        """
        self.stdout.write("Chargement de {:s}".format(filename))
        with open(os.path.join(data_dir, filename), 'r') as file:
            descriptions = json.load(file)
        objects = []
        nb_new = 0
        for description in descriptions:
            qset = klass.objects.filter(**description)
            try:
                objects.append(qset.get())
            except klass.DoesNotExist:
                obj = klass(**description)
                obj = callback(obj)
                obj.save()
                objects.append(obj)
                nb_new += 1
        self.stdout.write("- {:d} objets créés".format(nb_new))
        self.stdout.write("- {:d} objets gardés en l'état"
                          .format(len(objects)-nb_new))
        return objects
