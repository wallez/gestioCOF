# -*- coding: utf-8 -*-

from captcha.fields import ReCaptchaField

from django import forms
from django.forms import ModelForm
from django.forms.models import inlineformset_factory, BaseInlineFormSet
from django.contrib.auth.models import User

from gestioncof.petits_cours_models import PetitCoursDemande, PetitCoursAbility


class BaseMatieresFormSet(BaseInlineFormSet):
    def clean(self):
        super(BaseMatieresFormSet, self).clean()
        if any(self.errors):
            # Don't bother validating the formset unless each form is
            # valid on its own
            return
        matieres = []
        for i in range(0, self.total_form_count()):
            form = self.forms[i]
            if not form.cleaned_data:
                continue
            matiere = form.cleaned_data['matiere']
            niveau = form.cleaned_data['niveau']
            delete = form.cleaned_data['DELETE']
            if not delete and (matiere, niveau) in matieres:
                raise forms.ValidationError(
                    "Vous ne pouvez pas vous inscrire deux fois pour la "
                    "même matiere avec le même niveau.")
            matieres.append((matiere, niveau))


class DemandeForm(ModelForm):
    captcha = ReCaptchaField(attrs={'theme': 'clean', 'lang': 'fr'})

    def __init__(self, *args, **kwargs):
        super(DemandeForm, self).__init__(*args, **kwargs)
        self.fields['matieres'].help_text = ''

    class Meta:
        model = PetitCoursDemande
        fields = ('name', 'email', 'phone', 'quand', 'freq', 'lieu',
                  'matieres', 'agrege_requis', 'niveau', 'remarques')
        widgets = {'matieres': forms.CheckboxSelectMultiple}


MatieresFormSet = inlineformset_factory(
    User,
    PetitCoursAbility,
    fields=("matiere", "niveau", "agrege"),
    formset=BaseMatieresFormSet
)
