# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.forms.widgets import Widget
from django.forms.utils import flatatt
from django.utils.safestring import mark_safe


class TriStateCheckbox(Widget):
    def __init__(self, attrs=None, choices=()):
        super(TriStateCheckbox, self).__init__(attrs)
        # choices can be any iterable, but we may need to render this widget
        # multiple times. Thus, collapse it into a list so it can be consumed
        # more than once.
        self.choices = list(choices)

    def render(self, name, value, attrs=None, choices=()):
        if value is None:
            value = 'none'
        final_attrs = self.build_attrs(attrs, value=value)
        output = ["<span class=\"tristate\"%s></span>" % flatatt(final_attrs)]
        return mark_safe('\n'.join(output))
