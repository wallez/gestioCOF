from django.db import models
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save, post_delete

from gestioncof.petits_cours_models import choices_length

from bda.models import Spectacle

TYPE_COMMENT_FIELD = (
    ('text', _("Texte long")),
    ('char', _("Texte court")),
)


class CofProfile(models.Model):
    STATUS_EXTE = "exterieur"
    STATUS_1A = "1A"
    STATUS_2A = "2A"
    STATUS_3A = "3A"
    STATUS_4A = "4A"
    STATUS_ARCHI = "archicube"
    STATUS_DOCTORANT = "doctorant"
    STATUS_CST = "CST"
    STATUS_PEI = "PEI"

    OCCUPATION_CHOICES = (
        (STATUS_EXTE, _("Extérieur")),
        (STATUS_1A, _("1A")),
        (STATUS_2A, _("2A")),
        (STATUS_3A, _("3A")),
        (STATUS_4A, _("4A")),
        (STATUS_ARCHI, _("Archicube")),
        (STATUS_DOCTORANT, _("Doctorant")),
        (STATUS_CST, _("CST")),
        (STATUS_PEI, _("PEI")),
    )

    COTIZ_ETUDIANT = "etudiant"
    COTIZ_NORMALIEN = "normalien"
    COTIZ_EXTE = "exterieur"
    COTIZ_GRATIS = "gratis"

    TYPE_COTIZ_CHOICES = (
        (COTIZ_ETUDIANT, _("Normalien étudiant")),
        (COTIZ_NORMALIEN, _("Normalien élève")),
        (COTIZ_EXTE, _("Extérieur")),
        (COTIZ_GRATIS, _("Gratuit")),
    )

    user = models.OneToOneField(User, related_name="profile")
    login_clipper = models.CharField(
        "Login clipper", max_length=32, blank=True
    )
    is_cof = models.BooleanField("Membre du COF", default=False)
    phone = models.CharField("Téléphone", max_length=20, blank=True)
    occupation = models.CharField(_("Occupation"),
                                  default="1A",
                                  choices=OCCUPATION_CHOICES,
                                  max_length=choices_length(
                                      OCCUPATION_CHOICES))
    departement = models.CharField(_("Département"), max_length=50,
                                   blank=True)
    type_cotiz = models.CharField(_("Type de cotisation"),
                                  default="normalien",
                                  choices=TYPE_COTIZ_CHOICES,
                                  max_length=choices_length(
                                      TYPE_COTIZ_CHOICES))
    mailing_cof = models.BooleanField("Recevoir les mails COF", default=False)
    mailing_bda = models.BooleanField("Recevoir les mails BdA", default=False)
    mailing_bda_revente = models.BooleanField(
        "Recevoir les mails de revente de places BdA", default=False)
    comments = models.TextField(
        "Commentaires visibles par l'utilisateur", blank=True)
    is_buro = models.BooleanField("Membre du Burô", default=False)
    petits_cours_accept = models.BooleanField(
        "Recevoir des petits cours", default=False)
    petits_cours_remarques = models.TextField(
        _("Remarques et précisions pour les petits cours"),
        blank=True, default="")

    class Meta:
        verbose_name = "Profil COF"
        verbose_name_plural = "Profils COF"

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        CofProfile.objects.get_or_create(user=instance)


@receiver(post_delete, sender=CofProfile)
def post_delete_user(sender, instance, *args, **kwargs):
    instance.user.delete()


class Club(models.Model):
    name = models.CharField("Nom", max_length=200, unique=True)
    description = models.TextField("Description", blank=True)
    respos = models.ManyToManyField(User, related_name="clubs_geres",
                                    blank=True)
    membres = models.ManyToManyField(User, related_name="clubs", blank=True)

    def __str__(self):
        return self.name


class Event(models.Model):
    title = models.CharField("Titre", max_length=200)
    location = models.CharField("Lieu", max_length=200)
    start_date = models.DateTimeField("Date de début", blank=True, null=True)
    end_date = models.DateTimeField("Date de fin", blank=True, null=True)
    description = models.TextField("Description", blank=True)
    image = models.ImageField("Image", blank=True, null=True,
                              upload_to="imgs/events/")
    registration_open = models.BooleanField("Inscriptions ouvertes",
                                            default=True)
    old = models.BooleanField("Archiver (événement fini)", default=False)

    class Meta:
        verbose_name = "Événement"

    def __str__(self):
        return self.title


class EventCommentField(models.Model):
    event = models.ForeignKey(Event, related_name="commentfields")
    name = models.CharField("Champ", max_length=200)
    fieldtype = models.CharField("Type", max_length=10,
                                 choices=TYPE_COMMENT_FIELD, default="text")
    default = models.TextField("Valeur par défaut", blank=True)

    class Meta:
        verbose_name = "Champ"

    def __str__(self):
        return self.name


class EventCommentValue(models.Model):
    commentfield = models.ForeignKey(EventCommentField, related_name="values")
    registration = models.ForeignKey("EventRegistration",
                                     related_name="comments")
    content = models.TextField("Contenu", blank=True, null=True)

    def __str__(self):
        return "Commentaire de %s" % self.commentfield


class EventOption(models.Model):
    event = models.ForeignKey(Event, related_name="options")
    name = models.CharField("Option", max_length=200)
    multi_choices = models.BooleanField("Choix multiples", default=False)

    class Meta:
        verbose_name = "Option"

    def __str__(self):
        return self.name


class EventOptionChoice(models.Model):
    event_option = models.ForeignKey(EventOption, related_name="choices")
    value = models.CharField("Valeur", max_length=200)

    class Meta:
        verbose_name = "Choix"
        verbose_name_plural = "Choix"

    def __str__(self):
        return self.value


class EventRegistration(models.Model):
    user = models.ForeignKey(User)
    event = models.ForeignKey(Event)
    options = models.ManyToManyField(EventOptionChoice)
    filledcomments = models.ManyToManyField(EventCommentField,
                                            through=EventCommentValue)
    paid = models.BooleanField("A payé", default=False)

    class Meta:
        verbose_name = "Inscription"
        unique_together = ("user", "event")

    def __str__(self):
        return "Inscription de {} à {}".format(self.user, self.event.title)


class Survey(models.Model):
    title = models.CharField("Titre", max_length=200)
    details = models.TextField("Détails", blank=True)
    survey_open = models.BooleanField("Sondage ouvert", default=True)
    old = models.BooleanField("Archiver (sondage fini)", default=False)

    class Meta:
        verbose_name = "Sondage"

    def __str__(self):
        return self.title


class SurveyQuestion(models.Model):
    survey = models.ForeignKey(Survey, related_name="questions")
    question = models.CharField("Question", max_length=200)
    multi_answers = models.BooleanField("Choix multiples", default=False)

    class Meta:
        verbose_name = "Question"

    def __str__(self):
        return self.question


class SurveyQuestionAnswer(models.Model):
    survey_question = models.ForeignKey(SurveyQuestion, related_name="answers")
    answer = models.CharField("Réponse", max_length=200)

    class Meta:
        verbose_name = "Réponse"

    def __str__(self):
        return self.answer


class SurveyAnswer(models.Model):
    user = models.ForeignKey(User)
    survey = models.ForeignKey(Survey)
    answers = models.ManyToManyField(SurveyQuestionAnswer,
                                     related_name="selected_by")

    class Meta:
        verbose_name = "Réponses"
        unique_together = ("user", "survey")

    def __str__(self):
        return "Réponse de %s sondage %s" % (
                self.user.get_full_name(),
                self.survey.title)


class CalendarSubscription(models.Model):
    token = models.UUIDField()
    user = models.OneToOneField(User)
    other_shows = models.ManyToManyField(Spectacle)
    subscribe_to_events = models.BooleanField(default=True)
    subscribe_to_my_shows = models.BooleanField(default=True)

    def __str__(self):
        return "Calendrier de %s" % self.user.get_full_name()
