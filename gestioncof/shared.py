from django.conf import settings
from django.contrib.sites.models import Site

from django_cas_ng.backends import CASBackend

from gestioncof.models import CofProfile


class COFCASBackend(CASBackend):

    def clean_username(self, username):
        # Le CAS de l'ENS accepte les logins avec des espaces au début
        # et à la fin, ainsi qu’avec une casse variable. On normalise pour
        # éviter les doublons.
        return username.strip().lower()

    def configure_user(self, user):
        clipper = user.username
        user.profile.login_clipper = clipper
        user.profile.save()
        user.email = settings.CAS_EMAIL_FORMAT % clipper
        user.save()
        return user


def context_processor(request):
    '''Append extra data to the context of the given request'''
    data = {
        "user": request.user,
        "site": Site.objects.get_current(),
    }
    return data
