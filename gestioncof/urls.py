# -*- coding: utf-8 -*-

from django.conf.urls import url
from gestioncof.petits_cours_views import DemandeListView, DemandeDetailView
from gestioncof import views, petits_cours_views
from gestioncof.decorators import buro_required

export_patterns = [
    url(r'^members$', views.export_members),
    url(r'^mega/avecremarques$', views.export_mega_remarksonly),
    url(r'^mega/participants$', views.export_mega_participants),
    url(r'^mega/orgas$', views.export_mega_orgas),
    # url(r'^mega/(?P<type>.+)$', views.export_mega_bytype),
    url(r'^mega$', views.export_mega),
]

petitcours_patterns = [
    url(r'^inscription$', petits_cours_views.inscription,
        name='petits-cours-inscription'),
    url(r'^demande$', petits_cours_views.demande,
        name='petits-cours-demande'),
    url(r'^demande-raw$', petits_cours_views.demande_raw,
        name='petits-cours-demande-raw'),
    url(r'^demandes$',
        buro_required(DemandeListView.as_view()),
        name='petits-cours-demandes-list'),
    url(r'^demandes/(?P<pk>\d+)$',
        buro_required(DemandeDetailView.as_view()),
        name='petits-cours-demande-details'),
    url(r'^demandes/(?P<demande_id>\d+)/traitement$',
        petits_cours_views.traitement,
        name='petits-cours-demande-traitement'),
    url(r'^demandes/(?P<demande_id>\d+)/retraitement$',
        petits_cours_views.retraitement,
        name='petits-cours-demande-retraitement'),
]

surveys_patterns = [
    url(r'^(?P<survey_id>\d+)/status$', views.survey_status),
    url(r'^(?P<survey_id>\d+)$', views.survey),
]

events_patterns = [
    url(r'^(?P<event_id>\d+)$', views.event),
    url(r'^(?P<event_id>\d+)/status$', views.event_status),
]

calendar_patterns = [
    url(r'^subscription$', 'gestioncof.views.calendar'),
    url(r'^(?P<token>[a-z0-9-]+)/calendar.ics$',
        'gestioncof.views.calendar_ics')
]

clubs_patterns = [
    url(r'^membres/(?P<name>\w+)', views.membres_club, name='membres-club'),
    url(r'^liste', views.liste_clubs, name='liste-clubs'),
    url(r'^change_respo/(?P<club_name>\w+)/(?P<user_id>\d+)',
        views.change_respo, name='change-respo'),
]
