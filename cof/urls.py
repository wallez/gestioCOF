# -*- coding: utf-8 -*-

"""
Fichier principal de configuration des urls du projet GestioCOF
"""

import autocomplete_light

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic.base import TemplateView
from django.contrib.auth import views as django_views
from django_cas_ng import views as django_cas_views

from wagtail.wagtailadmin import urls as wagtailadmin_urls
from wagtail.wagtailcore import urls as wagtail_urls
from wagtail.wagtaildocs import urls as wagtaildocs_urls

from gestioncof import views as gestioncof_views, csv_views
from gestioncof.urls import export_patterns, petitcours_patterns, \
    surveys_patterns, events_patterns, calendar_patterns, \
    clubs_patterns
from gestioncof.autocomplete import autocomplete

autocomplete_light.autodiscover()
admin.autodiscover()

urlpatterns = [
    # Page d'accueil
    url(r'^$', gestioncof_views.home, name='home'),
    # Le BdA
    url(r'^bda/', include('bda.urls')),
    # Les exports
    url(r'^export/', include(export_patterns)),
    # Les petits cours
    url(r'^petitcours/', include(petitcours_patterns)),
    # Les sondages
    url(r'^survey/', include(surveys_patterns)),
    # Evenements
    url(r'^event/', include(events_patterns)),
    # Calendrier
    url(r'^calendar/', include(calendar_patterns)),
    # Clubs
    url(r'^clubs/', include(clubs_patterns)),
    # Authentification
    url(r'^cof/denied$', TemplateView.as_view(template_name='cof-denied.html'),
        name="cof-denied"),
    url(r'^cas/login$', django_cas_views.login, name="cas_login_view"),
    url(r'^cas/logout$', django_cas_views.logout),
    url(r'^outsider/login$', gestioncof_views.login_ext),
    url(r'^outsider/logout$', django_views.logout, {'next_page': 'home'}),
    url(r'^login$', gestioncof_views.login, name="cof-login"),
    url(r'^logout$', gestioncof_views.logout, name="cof-logout"),
    # Infos persos
    url(r'^profile$', gestioncof_views.profile),
    url(r'^outsider/password-change$', django_views.password_change),
    url(r'^outsider/password-change-done$',
        django_views.password_change_done,
        name='password_change_done'),
    # Inscription d'un nouveau membre
    url(r'^registration$', gestioncof_views.registration),
    url(r'^registration/clipper/(?P<login_clipper>[\w-]+)/'
        r'(?P<fullname>.*)$',
        gestioncof_views.registration_form2, name="clipper-registration"),
    url(r'^registration/user/(?P<username>.+)$',
        gestioncof_views.registration_form2, name="user-registration"),
    url(r'^registration/empty$', gestioncof_views.registration_form2,
        name="empty-registration"),
    # Autocompletion
    url(r'^autocomplete/registration$', autocomplete),
    url(r'^autocomplete/', include('autocomplete_light.urls')),
    # Interface admin
    url(r'^admin/logout/', gestioncof_views.logout),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/(?P<app_label>[\d\w]+)/(?P<model_name>[\d\w]+)/csv/',
        csv_views.admin_list_export,
        {'fields': ['username', ]}),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^grappelli/', include('grappelli.urls')),
    # Liens utiles du COF et du BdA
    url(r'^utile_cof$', gestioncof_views.utile_cof),
    url(r'^utile_bda$', gestioncof_views.utile_bda),
    url(r'^utile_bda/bda_diff$', gestioncof_views.liste_bdadiff),
    url(r'^utile_cof/diff_cof$', gestioncof_views.liste_diffcof),
    url(r'^utile_bda/bda_revente$', gestioncof_views.liste_bdarevente),
    url(r'^k-fet/', include('kfet.urls')),
    url(r'^cms/', include(wagtailadmin_urls)),
    url(r'^documents/', include(wagtaildocs_urls)),
    # djconfig
    url(r"^config", gestioncof_views.ConfigUpdate.as_view()),
]

if 'debug_toolbar' in settings.INSTALLED_APPS:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]

if settings.DEBUG:
    # Si on est en production, MEDIA_ROOT est servi par Apache.
    # Il faut dire à Django de servir MEDIA_ROOT lui-même en développement.
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)

# Wagtail for uncatched
urlpatterns += [
    url(r'', include(wagtail_urls)),
]
