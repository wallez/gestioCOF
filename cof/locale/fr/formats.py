# -*- encoding: utf-8 -*-

"""
Formats français.
"""

from __future__ import unicode_literals

DATETIME_FORMAT = r'l j F Y \à H:i'
