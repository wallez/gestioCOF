from channels.routing import include


routing = [
    include('kfet.routing.routing', path=r'^/ws/k-fet'),
]
