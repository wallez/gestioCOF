"""
Django local settings for the cof project.
The settings that are not listed here are imported from .common
"""

import os

from .dev import *  # NOQA
from .dev import BASE_DIR


# Use sqlite for local development
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3")
    }
}

# Use the default cache backend for local development
CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.locmem.LocMemCache"
    }
}

# Use the default in memory asgi backend for local development
CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "asgiref.inmemory.ChannelLayer",
        "ROUTING": "cof.routing.routing",
    }
}

# No need to run collectstatic -> unset STATIC_ROOT
STATIC_ROOT = None
