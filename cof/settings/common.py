# -*- coding: utf-8 -*-
"""
Django common settings for cof project.

Everything which is supposed to be identical between the production server and
the local development server should be here.
"""

import os

try:
    from . import secret
except ImportError:
    raise ImportError(
        "The secret.py file is missing.\n"
        "For a development environment, simply copy secret_example.py"
    )


def import_secret(name):
    """
    Shorthand for importing a value from the secret module and raising an
    informative exception if a secret is missing.
    """
    try:
        return getattr(secret, name)
    except AttributeError:
        raise RuntimeError("Secret missing: {}".format(name))


SECRET_KEY = import_secret("SECRET_KEY")
ADMINS = import_secret("ADMINS")
SERVER_EMAIL = import_secret("SERVER_EMAIL")

DBNAME = import_secret("DBNAME")
DBUSER = import_secret("DBUSER")
DBPASSWD = import_secret("DBPASSWD")

REDIS_PASSWD = import_secret("REDIS_PASSWD")
REDIS_DB = import_secret("REDIS_DB")
REDIS_HOST = import_secret("REDIS_HOST")
REDIS_PORT = import_secret("REDIS_PORT")

RECAPTCHA_PUBLIC_KEY = import_secret("RECAPTCHA_PUBLIC_KEY")
RECAPTCHA_PRIVATE_KEY = import_secret("RECAPTCHA_PRIVATE_KEY")

KFETOPEN_TOKEN = import_secret("KFETOPEN_TOKEN")


BASE_DIR = os.path.dirname(
    os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
)


# Application definition
INSTALLED_APPS = [
    'gestioncof',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'grappelli',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'bda',
    'autocomplete_light',
    'captcha',
    'django_cas_ng',
    'bootstrapform',
    'kfet',
    'kfet.open',
    'channels',
    'widget_tweaks',
    'custommail',
    'djconfig',
    'wagtail.wagtailforms',
    'wagtail.wagtailredirects',
    'wagtail.wagtailembeds',
    'wagtail.wagtailsites',
    'wagtail.wagtailusers',
    'wagtail.wagtailsnippets',
    'wagtail.wagtaildocs',
    'wagtail.wagtailimages',
    'wagtail.wagtailsearch',
    'wagtail.wagtailadmin',
    'wagtail.wagtailcore',
    'wagtail.contrib.modeladmin',
    'wagtailmenus',
    'modelcluster',
    'taggit',
    'kfet.cms',
]

MIDDLEWARE_CLASSES = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'kfet.middleware.KFetAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'djconfig.middleware.DjConfigMiddleware',
    'wagtail.wagtailcore.middleware.SiteMiddleware',
    'wagtail.wagtailredirects.middleware.RedirectMiddleware',
]

ROOT_URLCONF = 'cof.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.core.context_processors.i18n',
                'django.core.context_processors.media',
                'django.core.context_processors.static',
                'wagtailmenus.context_processors.wagtailmenus',
                'djconfig.context_processors.config',
                'gestioncof.shared.context_processor',
                'kfet.context_processors.auth',
                'kfet.context_processors.config',
            ],
        },
    },
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': DBNAME,
        'USER': DBUSER,
        'PASSWORD': DBPASSWD,
        'HOST': os.environ.get('DBHOST', 'localhost'),
    }
}


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'fr-fr'

TIME_ZONE = 'Europe/Paris'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Various additional settings
SITE_ID = 1

GRAPPELLI_ADMIN_HEADLINE = "GestioCOF"
GRAPPELLI_ADMIN_TITLE = "<a href=\"/\">GestioCOF</a>"

MAIL_DATA = {
    'petits_cours': {
        'FROM': "Le COF <cof@ens.fr>",
        'BCC': "archivescof@gmail.com",
        'REPLYTO': "cof@ens.fr"},
    'rappels': {
        'FROM': 'Le BdA <bda@ens.fr>',
        'REPLYTO': 'Le BdA <bda@ens.fr>'},
    'revente': {
        'FROM': 'BdA-Revente <bda-revente@ens.fr>',
        'REPLYTO': 'BdA-Revente <bda-revente@ens.fr>'},
}

LOGIN_URL = "cof-login"
LOGIN_REDIRECT_URL = "home"

CAS_SERVER_URL = 'https://cas.eleves.ens.fr/'
CAS_VERSION = '3'
CAS_LOGIN_MSG = None
CAS_IGNORE_REFERER = True
CAS_REDIRECT_URL = '/'
CAS_EMAIL_FORMAT = "%s@clipper.ens.fr"

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'gestioncof.shared.COFCASBackend',
    'kfet.backends.GenericTeamBackend',
)

RECAPTCHA_USE_SSL = True

# Cache settings

CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': 'redis://:{passwd}@{host}:{port}/db'
                    .format(passwd=REDIS_PASSWD, host=REDIS_HOST,
                            port=REDIS_PORT, db=REDIS_DB),
    }
}


# Channels settings

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "asgi_redis.RedisChannelLayer",
        "CONFIG": {
            "hosts": [(
                "redis://:{passwd}@{host}:{port}/{db}"
                .format(passwd=REDIS_PASSWD, host=REDIS_HOST,
                        port=REDIS_PORT, db=REDIS_DB)
            )],
        },
        "ROUTING": "cof.routing.routing",
    }
}

FORMAT_MODULE_PATH = 'cof.locale'

# Wagtail settings

WAGTAIL_SITE_NAME = 'GestioCOF'
WAGTAIL_ENABLE_UPDATE_CHECK = False
TAGGIT_CASE_INSENSITIVE = True
