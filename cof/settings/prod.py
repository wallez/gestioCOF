"""
Django development settings for the cof project.
The settings that are not listed here are imported from .common
"""

import os

from .common import *  # NOQA
from .common import BASE_DIR


DEBUG = False

ALLOWED_HOSTS = [
    "cof.ens.fr",
    "www.cof.ens.fr",
    "dev.cof.ens.fr"
]


STATIC_ROOT = os.path.join(
    os.path.dirname(os.path.dirname(BASE_DIR)),
    "public",
    "gestion",
    "static",
)

STATIC_URL = "/gestion/static/"
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), "media")
MEDIA_URL = "/gestion/media/"
