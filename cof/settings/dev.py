"""
Django development settings for the cof project.
The settings that are not listed here are imported from .common
"""

from .common import *  # NOQA
from .common import INSTALLED_APPS, MIDDLEWARE_CLASSES


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DEBUG = True


# ---
# Apache static/media config
# ---

STATIC_URL = '/static/'
STATIC_ROOT = '/srv/gestiocof/static/'

MEDIA_ROOT = '/srv/gestiocof/media/'
MEDIA_URL = '/media/'


# ---
# Debug tool bar
# ---

def show_toolbar(request):
    """
    On ne veut pas la vérification de INTERNAL_IPS faite par la debug-toolbar
    car cela interfère avec l'utilisation de Vagrant. En effet, l'adresse de la
    machine physique n'est pas forcément connue, et peut difficilement être
    mise dans les INTERNAL_IPS.
    """
    return DEBUG

INSTALLED_APPS += ["debug_toolbar", "debug_panel"]
MIDDLEWARE_CLASSES = (
    ["debug_panel.middleware.DebugPanelMiddleware"]
    + MIDDLEWARE_CLASSES
)
DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': show_toolbar,
}
