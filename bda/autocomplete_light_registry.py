# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import autocomplete_light

from bda.models import Participant, Spectacle

autocomplete_light.register(
    Participant, search_fields=('user__username', 'user__first_name',
                                'user__last_name'),
    autocomplete_js_attributes={'placeholder': 'participant...'})

autocomplete_light.register(
    Spectacle, search_fields=('title', ),
    autocomplete_js_attributes={'placeholder': 'spectacle...'})
