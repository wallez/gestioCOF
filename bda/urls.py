# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.conf.urls import url
from gestioncof.decorators import buro_required
from bda.views import SpectacleListView
from bda import views

urlpatterns = [
    url(r'^inscription/(?P<tirage_id>\d+)$',
        views.inscription,
        name='bda-tirage-inscription'),
    url(r'^places/(?P<tirage_id>\d+)$',
        views.places,
        name="bda-places-attribuees"),
    url(r'^revente/(?P<tirage_id>\d+)$',
        views.revente,
        name='bda-revente'),
    url(r'^etat-places/(?P<tirage_id>\d+)$',
        views.etat_places,
        name='bda-etat-places'),
    url(r'^tirage/(?P<tirage_id>\d+)$', views.tirage),
    url(r'^spectacles/(?P<tirage_id>\d+)$',
        buro_required(SpectacleListView.as_view()),
        name="bda-liste-spectacles"),
    url(r'^spectacles/(?P<tirage_id>\d+)/(?P<spectacle_id>\d+)$',
        views.spectacle,
        name="bda-spectacle"),
    url(r'^spectacles/unpaid/(?P<tirage_id>\d+)$',
        views.unpaid,
        name="bda-unpaid"),
    url(r'^liste-revente/(?P<tirage_id>\d+)$',
        views.list_revente,
        name="bda-liste-revente"),
    url(r'^buy-revente/(?P<spectacle_id>\d+)$',
        views.buy_revente,
        name="bda-buy-revente"),
    url(r'^revente-interested/(?P<revente_id>\d+)$',
        views.revente_interested,
        name='bda-revente-interested'),
    url(r'^revente-immediat/(?P<tirage_id>\d+)$',
        views.revente_shotgun,
        name="bda-shotgun"),
    url(r'^mails-rappel/(?P<spectacle_id>\d+)$',
        views.send_rappel,
        name="bda-rappels"
        ),
    url(r'^descriptions/(?P<tirage_id>\d+)$', views.descriptions_spectacles,
        name='bda-descriptions'),
    url(r'^catalogue/(?P<request_type>[a-z]+)$', views.catalogue,
        name='bda-catalogue'),
]
