# -*- coding: utf-8 -*-

from django import forms
from django.forms.models import BaseInlineFormSet
from django.utils import timezone

from bda.models import Attribution, Spectacle


class InscriptionInlineFormSet(BaseInlineFormSet):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # self.instance is a Participant object
        tirage = self.instance.tirage

        # set once for all "spectacle" field choices
        # - restrict choices to the spectacles of this tirage
        # - force_choices avoid many db requests
        spectacles = tirage.spectacle_set.select_related('location')
        choices = [(sp.pk, str(sp)) for sp in spectacles]
        self.force_choices('spectacle', choices)

    def force_choices(self, name, choices):
        """Set choices of a field.

        As ModelChoiceIterator (default use to get choices of a
        ModelChoiceField), it appends an empty selection if requested.

        """
        for form in self.forms:
            field = form.fields[name]
            if field.empty_label is not None:
                field.choices = [('', field.empty_label)] + choices
            else:
                field.choices = choices


class TokenForm(forms.Form):
    token = forms.CharField(widget=forms.widgets.Textarea())


class AttributionModelMultipleChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return "%s" % str(obj.spectacle)


class ResellForm(forms.Form):
    attributions = AttributionModelMultipleChoiceField(
                         label='',
                         queryset=Attribution.objects.none(),
                         widget=forms.CheckboxSelectMultiple,
                         required=False)

    def __init__(self, participant, *args, **kwargs):
        super(ResellForm, self).__init__(*args, **kwargs)
        self.fields['attributions'].queryset = (
            participant.attribution_set
            .filter(spectacle__date__gte=timezone.now())
            .exclude(revente__seller=participant)
            .select_related('spectacle', 'spectacle__location',
                            'participant__user')
        )


class AnnulForm(forms.Form):
    attributions = AttributionModelMultipleChoiceField(
                         label='',
                         queryset=Attribution.objects.none(),
                         widget=forms.CheckboxSelectMultiple,
                         required=False)

    def __init__(self, participant, *args, **kwargs):
        super(AnnulForm, self).__init__(*args, **kwargs)
        self.fields['attributions'].queryset = (
            participant.attribution_set
            .filter(spectacle__date__gte=timezone.now(),
                    revente__isnull=False,
                    revente__notif_sent=False,
                    revente__soldTo__isnull=True)
            .select_related('spectacle', 'spectacle__location',
                            'participant__user')
        )


class InscriptionReventeForm(forms.Form):
    spectacles = forms.ModelMultipleChoiceField(
                    queryset=Spectacle.objects.none(),
                    widget=forms.CheckboxSelectMultiple,
                    required=False)

    def __init__(self, tirage, *args, **kwargs):
        super(InscriptionReventeForm, self).__init__(*args, **kwargs)
        self.fields['spectacles'].queryset = (
            tirage.spectacle_set
            .select_related('location')
            .filter(date__gte=timezone.now())
        )


class SoldForm(forms.Form):
    attributions = AttributionModelMultipleChoiceField(
                        label='',
                        queryset=Attribution.objects.none(),
                        widget=forms.CheckboxSelectMultiple)

    def __init__(self, participant, *args, **kwargs):
        super(SoldForm, self).__init__(*args, **kwargs)
        self.fields['attributions'].queryset = (
            participant.attribution_set
            .filter(revente__isnull=False,
                    revente__soldTo__isnull=False)
            .exclude(revente__soldTo=participant)
            .select_related('spectacle', 'spectacle__location',
                            'participant__user')
        )
