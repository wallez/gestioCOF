# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('bda', '0008_py3'),
    ]

    operations = [
        migrations.CreateModel(
            name='SpectacleRevente',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True,
                                        auto_created=True, verbose_name='ID')),
                ('date', models.DateTimeField(
                    verbose_name='Date de mise en vente',
                    default=django.utils.timezone.now)),
                ('notif_sent', models.BooleanField(
                    verbose_name='Notification envoyée', default=False)),
                ('tirage_done', models.BooleanField(
                    verbose_name='Tirage effectué', default=False)),
            ],
            options={
                'verbose_name': 'Revente',
            },
        ),
        migrations.AddField(
            model_name='participant',
            name='choicesrevente',
            field=models.ManyToManyField(to='bda.Spectacle',
                                         related_name='subscribed',
                                         blank=True),
        ),
        migrations.AddField(
            model_name='spectaclerevente',
            name='answered_mail',
            field=models.ManyToManyField(to='bda.Participant',
                                         related_name='wanted',
                                         blank=True),
        ),
        migrations.AddField(
            model_name='spectaclerevente',
            name='attribution',
            field=models.OneToOneField(to='bda.Attribution',
                                       related_name='revente'),
        ),
        migrations.AddField(
            model_name='spectaclerevente',
            name='seller',
            field=models.ForeignKey(to='bda.Participant',
                                    verbose_name='Vendeur',
                                    related_name='original_shows'),
        ),
        migrations.AddField(
            model_name='spectaclerevente',
            name='soldTo',
            field=models.ForeignKey(to='bda.Participant',
                                    verbose_name='Vendue à', null=True,
                                    blank=True),
        ),
    ]
