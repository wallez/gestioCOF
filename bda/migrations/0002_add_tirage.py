# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
from django.utils import timezone

def forwards_func(apps, schema_editor):
    Tirage = apps.get_model("bda", "Tirage")
    db_alias = schema_editor.connection.alias
    Tirage.objects.using(db_alias).bulk_create([
        Tirage(
            id=1,
            title="Tirage de test (migration)",
            active=False,
            ouverture=timezone.now(),
            fermeture=timezone.now()),
    ])

class Migration(migrations.Migration):

    dependencies = [
        ('bda', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tirage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=300, verbose_name=b'Titre')),
                ('ouverture', models.DateTimeField(verbose_name=b"Date et heure d'ouverture du tirage")),
                ('fermeture', models.DateTimeField(verbose_name=b'Date et heure de fermerture du tirage')),
                ('token', models.TextField(verbose_name=b'Graine du tirage', blank=True)),
                ('active', models.BooleanField(default=True, verbose_name=b'Tirage actif')),
            ],
        ),
        migrations.RunPython(forwards_func, migrations.RunPython.noop),
        migrations.AlterField(
            model_name='participant',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='participant',
            name='tirage',
            field=models.ForeignKey(default=1, to='bda.Tirage'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='spectacle',
            name='tirage',
            field=models.ForeignKey(default=1, to='bda.Tirage'),
            preserve_default=False,
        ),
    ]
