# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bda', '0002_add_tirage'),
    ]

    operations = [
        migrations.AlterField(
            model_name='spectacle',
            name='price',
            field=models.FloatField(verbose_name=b"Prix d'une place"),
        ),
        migrations.AlterField(
            model_name='tirage',
            name='active',
            field=models.BooleanField(default=False, verbose_name=b'Tirage actif'),
        ),
    ]
