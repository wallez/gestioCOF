# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bda', '0003_update_tirage_and_spectacle'),
    ]

    operations = [
        migrations.AddField(
            model_name='spectacle',
            name='listing',
            field=models.BooleanField(default=False, verbose_name=b'Les places sont sur listing'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='spectacle',
            name='rappel_sent',
            field=models.DateTimeField(null=True, verbose_name=b'Mail de rappel envoy\xc3\xa9', blank=True),
        ),
    ]
