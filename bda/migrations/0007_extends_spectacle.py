# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bda', '0006_add_tirage_switch'),
    ]

    operations = [
        migrations.CreateModel(
            name='CategorieSpectacle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False,
                                        auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='Nom',
                                          unique=True)),
            ],
            options={
                'verbose_name': 'Cat\xe9gorie',
            },
        ),
        migrations.CreateModel(
            name='Quote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False,
                                        auto_created=True, primary_key=True)),
                ('text', models.TextField(verbose_name='Citation')),
                ('author', models.CharField(max_length=200,
                                            verbose_name='Auteur')),
            ],
        ),
        migrations.AlterModelOptions(
            name='spectacle',
            options={'ordering': ('date', 'title'),
                     'verbose_name': 'Spectacle'},
        ),
        migrations.RemoveField(
            model_name='spectacle',
            name='priority',
        ),
        migrations.AddField(
            model_name='spectacle',
            name='ext_link',
            field=models.CharField(
                max_length=500,
                verbose_name='Lien vers le site du spectacle',
                blank=True),
        ),
        migrations.AddField(
            model_name='spectacle',
            name='image',
            field=models.ImageField(upload_to='imgs/shows/', null=True,
                                    verbose_name='Image', blank=True),
        ),
        migrations.AlterField(
            model_name='tirage',
            name='enable_do_tirage',
            field=models.BooleanField(
                default=False,
                verbose_name='Le tirage peut \xeatre lanc\xe9'),
        ),
        migrations.AlterField(
            model_name='tirage',
            name='tokens',
            field=models.TextField(verbose_name='Graine(s) du tirage',
                                   blank=True),
        ),
        migrations.AddField(
            model_name='spectacle',
            name='category',
            field=models.ForeignKey(blank=True, to='bda.CategorieSpectacle',
                                    null=True),
        ),
        migrations.AddField(
            model_name='spectacle',
            name='vips',
            field=models.TextField(verbose_name='Personnalit\xe9s',
                                   blank=True),
        ),
        migrations.AddField(
            model_name='quote',
            name='spectacle',
            field=models.ForeignKey(to='bda.Spectacle'),
        ),
    ]
