# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils import timezone
from datetime import timedelta


def forwards_func(apps, schema_editor):
    SpectacleRevente = apps.get_model("bda", "SpectacleRevente")

    for revente in SpectacleRevente.objects.all():
        is_expired = timezone.now() > revente.date_tirage()
        is_direct = (revente.attribution.spectacle.date >= revente.date and
                     timezone.now() > revente.date + timedelta(minutes=15))
        revente.shotgun = is_expired or is_direct
        revente.save()


class Migration(migrations.Migration):

    dependencies = [
        ('bda', '0009_revente'),
    ]

    operations = [
        migrations.AddField(
            model_name='spectaclerevente',
            name='shotgun',
            field=models.BooleanField(default=False, verbose_name='Disponible imm\xe9diatement'),
        ),
        migrations.RunPython(forwards_func, migrations.RunPython.noop),
    ]
