# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bda', '0004_mails-rappel'),
    ]

    operations = [
        migrations.AlterField(
            model_name='choixspectacle',
            name='priority',
            field=models.PositiveIntegerField(verbose_name='Priorit\xe9'),
        ),
        migrations.AlterField(
            model_name='spectacle',
            name='priority',
            field=models.IntegerField(default=1000, verbose_name='Priorit\xe9'),
        ),
        migrations.AlterField(
            model_name='spectacle',
            name='rappel_sent',
            field=models.DateTimeField(null=True, verbose_name='Mail de rappel envoy\xe9', blank=True),
        ),
    ]
