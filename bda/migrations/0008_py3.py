# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bda', '0007_extends_spectacle'),
    ]

    operations = [
        migrations.AlterField(
            model_name='choixspectacle',
            name='double_choice',
            field=models.CharField(
                verbose_name='Nombre de places',
                choices=[('1', '1 place'),
                         ('autoquit', '2 places si possible, 1 sinon'),
                         ('double', '2 places sinon rien')],
                max_length=10, default='1'),
        ),
        migrations.AlterField(
            model_name='participant',
            name='paymenttype',
            field=models.CharField(
                blank=True,
                choices=[('cash', 'Cash'), ('cb', 'CB'),
                         ('cheque', 'Chèque'), ('autre', 'Autre')],
                max_length=6, verbose_name='Moyen de paiement'),
        ),
        migrations.AlterField(
            model_name='salle',
            name='address',
            field=models.TextField(verbose_name='Adresse'),
        ),
        migrations.AlterField(
            model_name='salle',
            name='name',
            field=models.CharField(verbose_name='Nom', max_length=300),
        ),
        migrations.AlterField(
            model_name='spectacle',
            name='date',
            field=models.DateTimeField(verbose_name='Date & heure'),
        ),
        migrations.AlterField(
            model_name='spectacle',
            name='description',
            field=models.TextField(verbose_name='Description', blank=True),
        ),
        migrations.AlterField(
            model_name='spectacle',
            name='listing',
            field=models.BooleanField(
                verbose_name='Les places sont sur listing'),
        ),
        migrations.AlterField(
            model_name='spectacle',
            name='price',
            field=models.FloatField(verbose_name="Prix d'une place"),
        ),
        migrations.AlterField(
            model_name='spectacle',
            name='slots',
            field=models.IntegerField(verbose_name='Places'),
        ),
        migrations.AlterField(
            model_name='spectacle',
            name='slots_description',
            field=models.TextField(verbose_name='Description des places',
                                   blank=True),
        ),
        migrations.AlterField(
            model_name='spectacle',
            name='title',
            field=models.CharField(verbose_name='Titre', max_length=300),
        ),
        migrations.AlterField(
            model_name='tirage',
            name='active',
            field=models.BooleanField(verbose_name='Tirage actif',
                                      default=False),
        ),
        migrations.AlterField(
            model_name='tirage',
            name='fermeture',
            field=models.DateTimeField(
                verbose_name='Date et heure de fermerture du tirage'),
        ),
        migrations.AlterField(
            model_name='tirage',
            name='ouverture',
            field=models.DateTimeField(
                verbose_name="Date et heure d'ouverture du tirage"),
        ),
        migrations.AlterField(
            model_name='tirage',
            name='title',
            field=models.CharField(verbose_name='Titre', max_length=300),
        ),
    ]
