# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bda', '0010_spectaclerevente_shotgun'),
    ]

    operations = [
        migrations.AddField(
            model_name='tirage',
            name='appear_catalogue',
            field=models.BooleanField(
                default=False,
                verbose_name='Tirage à afficher dans le catalogue'
            ),
        ),
    ]
