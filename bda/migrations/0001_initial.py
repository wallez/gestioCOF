# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Attribution',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('given', models.BooleanField(default=False, verbose_name='Donn\xe9e')),
            ],
        ),
        migrations.CreateModel(
            name='ChoixSpectacle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('priority', models.PositiveIntegerField(verbose_name=b'Priorit\xc3\xa9')),
                ('double_choice', models.CharField(default=b'1', max_length=10, verbose_name=b'Nombre de places', choices=[(b'1', b'1 place'), (b'autoquit', b'2 places si possible, 1 sinon'), (b'double', b'2 places sinon rien')])),
            ],
            options={
                'ordering': ('priority',),
                'verbose_name': 'voeu',
                'verbose_name_plural': 'voeux',
            },
        ),
        migrations.CreateModel(
            name='Participant',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('paid', models.BooleanField(default=False, verbose_name='A pay\xe9')),
                ('paymenttype', models.CharField(blank=True, max_length=6, verbose_name='Moyen de paiement', choices=[(b'cash', 'Cash'), (b'cb', b'CB'), (b'cheque', 'Ch\xe8que'), (b'autre', 'Autre')])),
            ],
        ),
        migrations.CreateModel(
            name='Salle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=300, verbose_name=b'Nom')),
                ('address', models.TextField(verbose_name=b'Adresse')),
            ],
        ),
        migrations.CreateModel(
            name='Spectacle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=300, verbose_name=b'Titre')),
                ('date', models.DateTimeField(verbose_name=b'Date & heure')),
                ('description', models.TextField(verbose_name=b'Description', blank=True)),
                ('slots_description', models.TextField(verbose_name=b'Description des places', blank=True)),
                ('price', models.FloatField(verbose_name=b"Prix d'une place", blank=True)),
                ('slots', models.IntegerField(verbose_name=b'Places')),
                ('priority', models.IntegerField(default=1000, verbose_name=b'Priorit\xc3\xa9')),
                ('location', models.ForeignKey(to='bda.Salle')),
            ],
            options={
                'ordering': ('priority', 'date', 'title'),
                'verbose_name': 'Spectacle',
            },
        ),
        migrations.AddField(
            model_name='participant',
            name='attributions',
            field=models.ManyToManyField(related_name='attributed_to', through='bda.Attribution', to='bda.Spectacle'),
        ),
        migrations.AddField(
            model_name='participant',
            name='choices',
            field=models.ManyToManyField(related_name='chosen_by', through='bda.ChoixSpectacle', to='bda.Spectacle'),
        ),
        migrations.AddField(
            model_name='participant',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='choixspectacle',
            name='participant',
            field=models.ForeignKey(to='bda.Participant'),
        ),
        migrations.AddField(
            model_name='choixspectacle',
            name='spectacle',
            field=models.ForeignKey(related_name='participants', to='bda.Spectacle'),
        ),
        migrations.AddField(
            model_name='attribution',
            name='participant',
            field=models.ForeignKey(to='bda.Participant'),
        ),
        migrations.AddField(
            model_name='attribution',
            name='spectacle',
            field=models.ForeignKey(related_name='attribues', to='bda.Spectacle'),
        ),
        migrations.AlterUniqueTogether(
            name='choixspectacle',
            unique_together=set([('participant', 'spectacle')]),
        ),
    ]
