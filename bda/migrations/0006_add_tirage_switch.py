# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.utils import timezone


def forwards_func(apps, schema_editor):
    Tirage = apps.get_model("bda", "Tirage")
    db_alias = schema_editor.connection.alias
    for tirage in Tirage.objects.using(db_alias).all():
        if tirage.tokens:
            tirage.tokens = "Before %s\n\"\"\"%s\"\"\"\n" % (
                    timezone.now().strftime("%y-%m-%d %H:%M:%S"),
                    tirage.tokens)
            tirage.save()


class Migration(migrations.Migration):

    dependencies = [
        ('bda', '0005_encoding'),
    ]

    operations = [
        migrations.RenameField('tirage', 'token', 'tokens'),
        migrations.AddField(
            model_name='tirage',
            name='enable_do_tirage',
            field=models.BooleanField(
                default=False,
                verbose_name=b'Le tirage peut \xc3\xaatre lanc\xc3\xa9'),
        ),
        migrations.RunPython(forwards_func, migrations.RunPython.noop),
    ]
