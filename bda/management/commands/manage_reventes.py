# -*- coding: utf-8 -*-

"""
Gestion en ligne de commande des reventes.
"""

from __future__ import unicode_literals

from datetime import timedelta
from django.core.management import BaseCommand
from django.utils import timezone
from bda.models import SpectacleRevente


class Command(BaseCommand):
    help = "Envoie les mails de notification et effectue " \
           "les tirages au sort des reventes"
    leave_locale_alone = True

    def handle(self, *args, **options):
        now = timezone.now()
        reventes = SpectacleRevente.objects.all()
        for revente in reventes:
            # Check si < 24h
            if (revente.attribution.spectacle.date <=
                    revente.date + timedelta(days=1)) and \
                    now >= revente.date + timedelta(minutes=15) and \
                    not revente.notif_sent:
                self.stdout.write(str(now))
                revente.mail_shotgun()
                self.stdout.write("Mail de disponibilité immédiate envoyé")
            # Check si délai de retrait dépassé
            elif (now >= revente.date + timedelta(hours=1) and
                  not revente.notif_sent):
                self.stdout.write(str(now))
                revente.send_notif()
                self.stdout.write("Mail d'inscription à une revente envoyé")
            # Check si tirage à faire
            elif (now >= revente.date_tirage and
                  not revente.tirage_done):
                self.stdout.write(str(now))
                revente.tirage()
                self.stdout.write("Tirage effectué, mails envoyés")
