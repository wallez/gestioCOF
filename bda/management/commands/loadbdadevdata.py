"""
Crée deux tirages de test et y inscrit les utilisateurs
"""

import os
import random

from django.utils import timezone
from django.contrib.auth.models import User

from gestioncof.management.base import MyBaseCommand
from bda.models import Tirage, Spectacle, Salle, Participant, ChoixSpectacle
from bda.views import do_tirage


# Où sont stockés les fichiers json
DATA_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)),
                        'data')


class Command(MyBaseCommand):
    help = "Crée deux tirages de test et y inscrit les utilisateurs."

    def handle(self, *args, **options):
        # ---
        # Tirages
        # ---

        Tirage.objects.all().delete()
        Tirage.objects.bulk_create([
            Tirage(
                title="Tirage de test 1",
                ouverture=timezone.now()-timezone.timedelta(days=7),
                fermeture=timezone.now(),
                active=True
            ),
            Tirage(
                title="Tirage de test 2",
                ouverture=timezone.now(),
                fermeture=timezone.now()+timezone.timedelta(days=60),
                active=True
            )
        ])
        tirages = Tirage.objects.all()

        # ---
        # Salles
        # ---

        locations = self.from_json('locations.json', DATA_DIR, Salle)

        # ---
        # Spectacles
        # ---

        def show_callback(show):
            """
            Assigne un tirage, une date et un lieu à un spectacle et décide si
            les places sont sur listing.
            """
            show.tirage = random.choice(tirages)
            show.listing = bool(random.randint(0, 1))
            show.date = (
                show.tirage.fermeture
                + timezone.timedelta(days=random.randint(60, 90))
            )
            show.location = random.choice(locations)
            return show
        shows = self.from_json(
            'shows.json', DATA_DIR, Spectacle, show_callback
        )

        # ---
        # Inscriptions
        # ---

        self.stdout.write("Inscription des utilisateurs aux tirages")
        ChoixSpectacle.objects.all().delete()
        choices = []
        for user in User.objects.filter(profile__is_cof=True):
            for tirage in tirages:
                part, _ = Participant.objects.get_or_create(
                    user=user,
                    tirage=tirage
                )
                shows = random.sample(
                    list(tirage.spectacle_set.all()),
                    tirage.spectacle_set.count() // 2
                )
                for (rank, show) in enumerate(shows):
                    choices.append(ChoixSpectacle(
                        participant=part,
                        spectacle=show,
                        priority=rank + 1,
                        double_choice=random.choice(
                            ['1', 'double', 'autoquit']
                        )
                    ))
        ChoixSpectacle.objects.bulk_create(choices)
        self.stdout.write("- {:d} inscriptions générées".format(len(choices)))

        # ---
        # On lance le premier tirage
        # ---

        self.stdout.write("Lancement du premier tirage")
        do_tirage(tirages[0], "dummy_token")
