# Tâches cron à mettre sur la VM en production

---

## Envoi des mails de rappel

Il faut trigger de temps en temps la commande `sendrappels` de GestioCOF qui
envoie les mails de rappels des spectacles à venir (sauf s'ils ont déjà été
envoyés).

- Un fois toutes les 12 heures me semble bien.
- Penser à utiliser le bon executable python (virtualenvs) et le bon fichier de
  settings pour Django.
- Garder les logs peut être une bonne idée.

Exemple : voir le fichier `provisioning/cron.dev`.

## Gestion des mails de revente

Il faut effectuer très régulièrement la commande `manage_reventes` de GestioCOF,
qui gère toutes les actions associées à BdA-Revente : envoi des mails de notification,
tirages.

- Pour l'instant un délai de 5 min est hardcodé
- Garde des logs ; ils vont finir par être assez lourds si on a beaucoup de reventes.

Exemple : provisioning/cron.dev
