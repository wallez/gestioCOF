#!/bin/sh

# Configuration de la base de données. Le mot de passe est constant car c'est
# pour une installation de dév locale qui ne sera accessible que depuis la
# machine virtuelle.
DBUSER="cof_gestion"
DBNAME="cof_gestion"
DBPASSWD="4KZt3nGPLVeWSvtBZPSM3fSzXpzEU4"

# Installation de paquets utiles
apt-get update && apt-get upgrade -y
apt-get install -y python3-pip python3-dev python3-venv libpq-dev postgresql \
                   postgresql-contrib libjpeg-dev nginx git redis-server

# Postgresql
sudo -u postgres createdb $DBNAME
sudo -u postgres createuser -SdR $DBUSER
sudo -u postgres psql -c "ALTER USER $DBUSER WITH PASSWORD '$DBPASSWD';"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE $DBNAME TO $DBUSER;"


# Redis
REDIS_PASSWD="dummy"
redis-cli CONFIG SET requirepass $REDIS_PASSWD
redis-cli -a $REDIS_PASSWD CONFIG REWRITE

# Contenu statique
mkdir -p /srv/gestiocof/media
mkdir -p /srv/gestiocof/static
chown -R ubuntu:www-data /srv/gestiocof

# Nginx
ln -s -f /vagrant/provisioning/nginx.conf /etc/nginx/sites-enabled/gestiocof.conf
rm -f /etc/nginx/sites-enabled/default
systemctl reload nginx

# Environnement virtuel python
sudo -H -u ubuntu python3 -m venv ~ubuntu/venv
sudo -H -u ubuntu ~ubuntu/venv/bin/pip install -U pip
sudo -H -u ubuntu ~ubuntu/venv/bin/pip install -r /vagrant/requirements-devel.txt

# Préparation de Django
cd /vagrant
ln -s -f secret_example.py cof/settings/secret.py
sudo -H -u ubuntu \
  DJANGO_SETTINGS_MODULE='cof.settings.dev' \
  bash -c ". ~/venv/bin/activate && bash provisioning/prepare_django.sh"
/home/ubuntu/venv/bin/python manage.py collectstatic --noinput --settings cof.settings.dev

# Installation du cron pour les mails de rappels
sudo -H -u ubuntu crontab provisioning/cron.dev

# Daphne + runworker
cp /vagrant/provisioning/daphne.service /etc/systemd/system/daphne.service
cp /vagrant/provisioning/worker.service /etc/systemd/system/worker.service
systemctl enable daphne.service
systemctl enable worker.service
systemctl start daphne.service
systemctl start worker.service

# Mise en place du .bash_profile pour tout configurer lors du `vagrant ssh`
cat >> ~ubuntu/.bashrc <<EOF
# On utilise la version de développement de GestioCOF
export DJANGO_SETTINGS_MODULE='cof.settings.dev'

# Charge le virtualenv
source ~/venv/bin/activate

# On va dans /vagrant où se trouve le code de gestioCOF
cd /vagrant
EOF
